/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/bucket',
    'lib/image',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-math',
    'p!-error',
    'p!-logger',
    './bucketMetadata'
], function($Pool,
    $Bucket,
    $Image,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $math,
    $error,
    $logger) {
    'use strict';

    // //$error.__DEBUG__ = true;
    $Eventful.emitter.setMax(1000);

    describe('Bucket - $Image implementation', function() {
        var _imageSize = $Image.DEFAULT_CONFIG.image.size;
        var _imageSizeKB = _imageSize.width * _imageSize.height * 0.004 / $Image.DEFAULT_CONFIG.image.compressionRatio;

        var itemIndex = 0;
        function makeItem() {
            ++itemIndex;
            return $Image($Image.DEFAULT_URL + '?' + itemIndex);
        }

        describe('add|remove 1 item', function() {
            var _master, _item, _bucket;

            var _bucketConfig = {
                capacity: _imageSizeKB + 1
            };

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);

                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket');

                _master = _bucket.getMaster();

                _item = makeItem();
                _bucket.addItem(_item);
                _item.addListener('loaded', done);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should exist', function() {
                expect(_bucket).toBeDefined();
            });

            it('Should not throw when adding same item', function() {
                expect(_bucket.addItem.bind(_bucket, _item)).not.toThrow();
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have one item', function() {
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have memory equal to item', function() {
                expect(_bucket.getMemorySize()).toEqual(_item.getMemorySize());
            });

            it('Should have added item to master', function() {
                expect(_master.getMemorySize()).toEqual(_bucket.getMemorySize());
                expect(_master.getLength()).toEqual(_bucket.getLength());
                expect(_master.getArray()[0]).toEqual(_bucket.getArray()[0]);
            });

            it('Should not throw when removed item', function() {
                expect(_bucket.removeItem.bind(_bucket, _item)).not.toThrow();
            });

            it('Should have length of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getLength()).toEqual(0);
            });

            it('Should have not removed item from master', function() {
                _bucket.removeItem(_item);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have memory of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getMemorySize()).toEqual(0);
            });

            it('Should find item after its been removed', function() {
                expect(_bucket.removeItem(_item).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should find item after its been removed with force', function() {
                expect(_bucket.removeItem(_item, true).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should not find in bucket if it was destroyed', function() {
                _item.destroy();
                expect(_bucket.getLength()).toEqual(0);
            });

            it('Should not find in master if it was destroyed', function() {
                _item.destroy();
                expect(_master.getLength()).toEqual(0);
            });

            it('Should be destroyed if Master was deconstructed', function() {
                $Config.deinit();
                expect(_bucket.__timeCreated).toEqual(null);
            });
        });

        describe('multiple instances relation (1 item in 1 bucket only - among many buckets)', function() {
            var _item;

            var _bucketConfig = {
                capacity: _imageSizeKB + 1
            };
            var _bucketsAmount = 100;
            var _buckets = [];

            beforeAll(function(done) {
                var _bucket, _i;
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket (' + _i + ')');
                    _buckets.push(_bucket);
                }
                //
                _item = makeItem();
                //only one bucket has item
                _bucket.addItem(_item);
                //
                _item.addListener('loaded', done);
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
                _buckets = [];
            });

            it('Should have only one bucket the length of 1', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
            });

            it('Should link item to a bucket that exists in the master but does not in bucket when searching bucket', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    bucket.findItemByHash(_item.getHash());
                    //length should be updated if bucket had refferenced the item
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length);
            });
        });

        describe('multiple instances relation (same item in all buckets)', function() {
            var _master, _item;

            var _bucketConfig = {
                capacity: _imageSizeKB + 1
            };
            var _bucketsAmount = 100;
            var _buckets = [];

            beforeEach(function(done) {
                var _bucket, _i;
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _item = makeItem();

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket (' + _i + ')');
                    _buckets.push(_bucket);
                    _bucket.addItem(_item);
                }
                //get holf of master
                _master = _bucket.getMaster();
                //
                _item.addListener('loaded', done);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
                _buckets = [];
            });

            it('Should have length 1 in all buckets', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length);
            });

            it('Should have length of 1 in master', function() {
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in all buckets exept the one the iem was removed from', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket, _i) {
                    if (_i === 0) {
                        bucket.removeItem(_item);
                    }
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length - 1);
            });

            it('Should have length of 0 in all buckets and master when item is destroyed', function() {
                var _totalCount = 0;
                _item.destroy();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(0);
            });

            it('Should not be able to destroy item if any of the linked nodes is locked using item.destroy()', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.destroy();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_totalCount);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should be able to destroy item if any of the linked nodes is locked using item.destroy(force)', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.destroy(true);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(0);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using buckets regardless if bucket is locked or not', function() {
                var _totalCount = 0;
                _buckets[50].lock();

                _buckets.forEach(function(bucket) {
                    bucket.removeItem(_item);
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using master (all none locked refference will be removed)', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _master.removeItem(_item);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using item', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.removeFromBuckets();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 0 in locked buckets and 1 in master when item is removed using item - force', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.removeFromBuckets(true);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(1);
            });
        });

        describe('config', function() {
            var _masterConfig = {
                SYSTEM: {
                    capacity: 100
                }
            };
            beforeAll(function() {
                //set up masters
                $Config.init(_masterConfig);
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should throw if bucket capacity is bigger than its master', function() {
                expect($Bucket.bind(null, $Config($Config.TYPE.SYSTEM, {
                    capacity: 101
                }))).toThrow();
            });

            it('Should throw if no config was specified', function() {
                expect($Bucket.bind(null)).toThrow();
            });
        });

        describe('If overflown: ', function() {
            var _bucketCount = 2;
            var _itemsCount = 5;

            var _masterConfig = {
                SYSTEM: {
                    capacity: _imageSizeKB * _bucketCount * _itemsCount - 1
                }
            };

            var _bucketConfig = {
                capacity: _masterConfig.SYSTEM.capacity
            };

            var onDone;
            var totalLoads = 0;
            var totalItemsTriggerred = [];
            var master;

            function onImageLoaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            function onImageUnloaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            beforeAll(function(__done) {
                var _bucket, _item, _i, _b;
                var _itemCollection = [];

                onDone = __done;

                //set up masters
                $Config.init(_masterConfig);
                //save prototype
                //
                for (_i = 0; _i < _bucketCount; _i += 1) {
                    //
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (' + _i + ')');
                    _itemCollection.length = 0;
                    for (_b = 0; _b < _itemsCount; _b += 1) {
                        _item = makeItem();
                        _item.addListener('loaded', onImageLoaded);
                        _item.addListener('deconstructed', onImageUnloaded);
                        _itemCollection.push(_item);
                    }
                    //
                    _bucket.addArray(_itemCollection);
                }
                master = $Config.TYPE.SYSTEM.pool;
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
                totalLoads = 0;
                totalItemsTriggerred.length = 0;
                master = null;
            });

            it('Should have master clear its content to stay beneath the max', function() {
                expect(master.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
                expect(master.getLinkedItems().length).toBeGreaterThan(0);
            });

            it('Should have total bucket linked memory less than or equal to max master capacity', function() {
                var _size = $math.near(master.getLinkedSize(), master.getMemorySize()) ? master.getMemorySize() : master.getLinkedSize(); // HACK: only here so the test passes if linkedSize is > memorySize by a floating point precision error
                expect(_size).toBeLessThanOrEqual(master.getMemorySize());
            });
        });

        describe('locked (one of) - If overflown: ', function() {
            var _bucketCount = 2;
            var _itemsCount = 10;

            var _masterConfig = {
                SYSTEM: {
                    capacity: _imageSizeKB * (_bucketCount * _itemsCount - 1)
                }
            };

            var _bucketConfig = {
                capacity: _masterConfig.SYSTEM.capacity
            };

            var onDone;
            var totalLoads = 0;
            var totalItemsTriggerred = {};

            var master;

            function onImageLoaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            function onImageUnloaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }


            beforeAll(function(__done) {
                var _bucket, _item, _i;
                var _itemCollection = [];

                onDone = __done;

                //set up masters
                $Config.init(_masterConfig);


                //fill locked bucket
                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (locked)');
                _bucket.lock();
                for (_i = 0; _i < _itemsCount; _i += 1) {
                    _item = makeItem();
                    _item.addListener('loaded', onImageLoaded);
                    _item.addListener('deconstructed', onImageUnloaded);
                    _itemCollection.push(_item);
                }
                _bucket.addArray(_itemCollection);

                _itemCollection.length = 0;
                //fill locked bucket
                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (not locked)');
                for (_i = 0; _i < _itemsCount; _i += 1) {
                    _item = makeItem();
                    _item.addListener('loaded', onImageLoaded);
                    _item.addListener('deconstructed', onImageUnloaded);
                    _itemCollection.push(_item);
                }
                _bucket.addArray(_itemCollection);

                master = $Config.TYPE.SYSTEM.pool;
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
                totalLoads = 0;
                totalItemsTriggerred = null;
                master = null;
            });

            it('Should have master clear its content to stay beneath the max', function() {
                expect(master.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
                expect(master.getLinkedItems().length).toBeGreaterThan(0);
            });

            it('Should have total bucket linked memory less than or equal to max master capacity', function() {
                var _size = $math.near(master.getLinkedSize(), master.getMemorySize()) ? master.getMemorySize() : master.getLinkedSize(); // HACK: only here so the test passes if linkedSize is > memorySize by a floating point precision error
                expect(_size).toBeLessThanOrEqual(master.getMemorySize());
            });

            it('Should have locked bucket retain its items over the non locked', function() {
                var _lockedItems = 0;
                var _unlockedItems = 0;
                master.getBuckets().forEach(function(__bucket) {
                    if (__bucket.isLocked()) {
                        _lockedItems += __bucket.getLinkedItems().length;
                    } else {
                        _unlockedItems += __bucket.getLinkedItems().length;
                    }
                });
                expect(_lockedItems).toBeGreaterThan(_unlockedItems);
            });

            it('Should have non locked bucket not retain its items over the locked bucket', function() {
                var _lockedItems = 0;
                var _unlockedItems = 0;
                master.getBuckets().forEach(function(__bucket) {
                    if (__bucket.isLocked()) {
                        _lockedItems += __bucket.getLinkedItems().length;
                    } else {
                        _unlockedItems += __bucket.getLinkedItems().length;
                    }
                });
                expect(_unlockedItems).not.toBeGreaterThan(_lockedItems);
            });
        });
    });


    //TEXTURE TESTS

    describe('Bucket - $Texture implementation', function() {
        var _textureSize = $Texture.DEFAULT_CONFIG.texture.texture.size;
        var _textureGPUsizeKB = _textureSize.width * _textureSize.height * 0.004;
        var _compressionRatio = $Texture.DEFAULT_CONFIG.texture.image.compressionRatio;

        //override default config to account for VIDEO/SYSTEM MEMORY
        var _conf = {
            SYSTEM: {
                capacity: 100000
            },
            VIDEO: {
                capacity: 100000
            },
            HEAP: {
                capacity: 100000
            }
        };

        var itemIndex = 0;
        function makeItem() {
            ++itemIndex;
            return $Texture($Texture.DEFAULT_URL + '?' + itemIndex);
        }

        describe('add|remove 1 item', function() {
            var _master, _item, _bucket;

            var _bucketConfig = {
                capacity: _textureGPUsizeKB + 1
            };

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);

                _bucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket');

                _master = _bucket.getMaster();

                _item = makeItem();
                _bucket.addItem(_item);
                _item.addListener('loaded', done);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should exist', function() {
                expect(_bucket).toBeDefined();
            });
            it('Should not throw when adding same item', function() {
                expect(_bucket.addItem.bind(_bucket, _item)).not.toThrow();
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have one item', function() {
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have memory equal to item', function() {
                expect(_bucket.getMemorySize()).toEqual(_item.getMemorySize());
            });

            it('Should have added item to master', function() {
                expect(_master.getMemorySize()).toEqual(_bucket.getMemorySize());
                expect(_master.getLength()).toEqual(_bucket.getLength());
                expect(_master.getArray()[0]).toEqual(_bucket.getArray()[0]);
            });

            it('Should not throw when removed item', function() {
                expect(_bucket.removeItem.bind(_bucket, _item)).not.toThrow();
            });

            it('Should have length of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getLength()).toEqual(0);
            });

            it('Should have not removed item from master', function() {
                _bucket.removeItem(_item);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have memory of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getMemorySize()).toEqual(0);
            });

            it('Should find item after its been removed', function() {
                expect(_bucket.removeItem(_item).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should find item after its been removed with force', function() {
                expect(_bucket.removeItem(_item, true).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should not find in bucket if it was destroyed', function() {
                _item.destroy();
                expect(_bucket.getLength()).toEqual(0);
            });

            it('Should not find in master if it was destroyed', function() {
                _item.destroy();
                expect(_master.getLength()).toEqual(0);
            });

            it('Should be destroyed if Master was deconstructed', function() {
                $Config.deinit();
                expect(_bucket.__timeCreated).toEqual(null);
            });
        });

        describe('multiple instances relation (1 item in 1 bucket only - among "' + $Eventful.emitter.MAX_HANDLERS_PER_TYPE + '" buckets)', function() {
            var _bucketsAmount = $Eventful.emitter.MAX_HANDLERS_PER_TYPE;
            var _item;

            var _confMaster = {
                SYSTEM: {
                    capacity: _textureGPUsizeKB * _bucketsAmount / _compressionRatio //set the capacity such that images do not overflow
                },
                VIDEO: {
                    capacity: _textureGPUsizeKB * _bucketsAmount //set the capacity such that video DOES NOT overflow!
                }
            };
            //set buacket capacity same as master
            var _bucketConfig = {
                capacity: _confMaster.VIDEO.capacity
            };
            beforeAll(function(done) {
                var _bucket, _i;
                //set up masters
                $Config.init(_confMaster);
                //
                _item = makeItem();

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket (' + _i + ')');
                }

                //only one bucket has item
                _bucket.addItem(_item);
                //
                _item.addListener('loaded', done);
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should have only one bucket the length of 1', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
            });

            it('Should link item to a bucket that exists in the master but does not in bucket when searching bucket', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    bucket.findItemByHash(_item.getHash());
                    //length should be updated if bucket had refferenced the item
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual($Config.TYPE.VIDEO.pool.getBuckets().length);
            });
        });

        describe('multiple instances relation (same item in  "' + $Eventful.emitter.MAX_HANDLERS_PER_TYPE + '" buckets)', function() {
            var _bucketsAmount = $Eventful.emitter.MAX_HANDLERS_PER_TYPE;
            var _item;

            var _confMaster = {
                SYSTEM: {
                    capacity: _textureGPUsizeKB * _bucketsAmount / _compressionRatio //set the capacity such that images do not overflow
                },
                VIDEO: {
                    capacity: _textureGPUsizeKB * _bucketsAmount //set the capacity such that video DOES NOT overflow!
                }
            };
            //set buacket capacity same as master
            var _bucketConfig = {
                capacity: _confMaster.VIDEO.capacity
            };

            beforeEach(function(done) {
                var _bucket, _i;
                //set up masters
                $Config.init(_confMaster);
                _item = makeItem();

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket (' + _i + ')');
                    _bucket.addItem(_item);
                }
                //
                _item.addListener('loaded', done);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should have length 1 in all buckets', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual($Config.TYPE.VIDEO.pool.getBuckets().length);
            });

            it('Should have length of 1 in master', function() {
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });

            it('Should have length of 1 in all buckets exept the one the iem was removed from', function() {
                var _totalCount = 0;
                //remove item from one bucket
                $Config.TYPE.VIDEO.pool.getBuckets()[0].removeItem(_item);
                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual($Config.TYPE.VIDEO.pool.getBuckets().length - 1);
            });

            it('Should have length of 0 in all buckets and master when item is destroyed', function() {
                var _totalCount = 0;
                _item.destroy();

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
            });

            it('Should have length of 0 master when item is destroyed', function() {
                _item.destroy();
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(0);
            });

            it('Should not be able to destroy item if any of the linked nodes is locked using item.destroy()', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[0].lock();
                _item.destroy();

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_totalCount);
            });

            it('Should not be able to destroy item if any of the linked nodes is locked using item.destroy(), master retains one item', function() {
                $Config.TYPE.VIDEO.pool.getBuckets()[0].lock();
                _item.destroy();
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });

            it('Should be able to destroy item if any of the linked nodes is locked using item.destroy(force)', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[0].lock();
                _item.destroy(true);

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
            });

            it('Should be able to destroy item if any of the linked nodes is locked using item.destroy(force) in master', function() {
                $Config.TYPE.VIDEO.pool.getBuckets()[0].lock();
                _item.destroy(true);
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(0);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using buckets regardless if bucket is locked or not', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[50].lock();

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    bucket.removeItem(_item);
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using master (all none locked refference will be removed)', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[50].lock();
                $Config.TYPE.VIDEO.pool.removeItem(_item);

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using item', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[50].lock();
                _item.removeFromBuckets();

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });

            it('Should have length of 0 in locked buckets and 1 in master when item is removed using item - force', function() {
                var _totalCount = 0;
                $Config.TYPE.VIDEO.pool.getBuckets()[50].lock();
                _item.removeFromBuckets(true);

                $Config.TYPE.VIDEO.pool.getBuckets().forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect($Config.TYPE.VIDEO.pool.getLength()).toEqual(1);
            });
        });

        describe('config VIDEO', function() {
            var _masterConfig = {
                VIDEO: {
                    capacity: 100
                }
            };
            beforeAll(function() {
                //set up masters
                $Config.init(_masterConfig);
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should throw if bucket capacity is bigger than its master', function() {
                expect($Bucket.bind(null, $Config($Config.TYPE.VIDEO, {
                    capacity: 101
                }))).toThrow();
            });

            it('Should throw if no config was specified', function() {
                expect($Bucket.bind(null)).toThrow();
            });
        });

        describe('If VIDEO overflown: ', function() {
            var _bucketCount = 2;
            var _itemsCount = 50;

            var _confMaster = {
                SYSTEM: {
                    capacity: _textureGPUsizeKB * _itemsCount * _bucketCount / _compressionRatio //set the capacity such that images do not overflow
                },
                VIDEO: {
                    capacity: _textureGPUsizeKB * (_itemsCount * _bucketCount / 2) //set the capacity such that video DOES overflow!
                }
            };
            //set buacket capacity same as master
            var _bucketConfig = {
                capacity: _confMaster.VIDEO.capacity
            };

            var onDone;
            var totalLoads = 0;
            var totalItemsTriggerred = {};
            // var protoMemSizeImage = $Texture.prototype.getMemorySize;
            var master;
            var bucket;

            function onImageLoaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            function onImageUnloaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            function addItem(_bucket) {
                return new Promise(function(__resolve, __reject) {
                    var _item = makeItem();
                    //console.log(_bucket.name, _item);
                    _bucket.addItem(_item);
                    _item.addListener('loaded', function(__item) {
                        onImageLoaded(__item);
                        __resolve();
                    });
                    _item.addListener('deconstructed', function(__item) {
                        onImageUnloaded(__item);
                        __resolve();
                    });
                });
            }


            beforeAll(function(__done) {
                var _i, _b;
                var _p = Promise.resolve();

                onDone = __done;

                //set up masters
                $Config.init(_confMaster);
                for (_i = 0; _i < _bucketCount; _i += 1) {
                    bucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket: (' + _i + ')');
                    for (_b = 0; _b < _itemsCount; _b += 1) {
                        _p = _p.then(addItem.bind(undefined, bucket));
                    }
                }
                master = $Config.TYPE.VIDEO.pool;
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
                totalLoads = 0;
                totalItemsTriggerred = null;
                master = null;
                bucket = null;
            });

            it('Should have master clear its content to stay beneath or equal to the max capacity (<=' + _confMaster.VIDEO.capacity + 'KB)', function() {
                expect(master.getMemorySize()).toBeLessThanOrEqual(_confMaster.VIDEO.capacity);
            });

            it('Should have master clear its content and have less items than initially cache (<' + _itemsCount * _bucketCount + ')', function() {
                expect(master.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
            });

            it('Should have master clear its content but have some items cached', function() {
                expect(master.getLinkedItems().length).toBeGreaterThan(0);
            });

            it('Should have SYSTEM master NOT clear its content (' + _itemsCount * _bucketCount + ')', function() {
                expect($Config.TYPE.SYSTEM.pool.getLength()).toEqual(_itemsCount * _bucketCount);
            });

            it('Should have total bucket linked memory less than or equal to max master capacity (<=' + _confMaster.VIDEO.capacity + 'KB)', function() {
                expect(master.getLinkedSize()).toBeLessThanOrEqual(master.getMemorySize());
            });

            it('Should have total last bucket memory size be equal its master due to purge of item belong to the first bucket', function() {
                expect(bucket.getMemorySize()).toBeCloseTo(master.getMemorySize());
            });

            it('Should have first bucket length equal "0"', function() {
                expect(master.getBuckets()[0].getMemorySize()).toBeCloseTo(0);
            });
        });

        describe('locked (one of) - If overflown: ', function() {
            // $logger.enableTags($logger.TAGS.CACHE);
            var _bucketCount = 2;
            var _itemsCount = 10;

            var _confMaster = {
                SYSTEM: {
                    capacity: _textureGPUsizeKB * _itemsCount * _bucketCount / _compressionRatio //set the capacity such that images do not overflow
                },
                VIDEO: {
                    capacity: _textureGPUsizeKB * (_itemsCount * _bucketCount / 2) //set the capacity such that video DOES overflow!
                }
            };
            //set bucket capacity same as master
            var _bucketConfig = {
                capacity: _confMaster.VIDEO.capacity
            };

            var onDone;
            var totalLoads = 0;
            var totalItemsTriggerred = {};
            var master;
            var _lockedBucket;
            var _unlockedBucket;

            function onImageLoaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            function onImageUnloaded(__item) {
                if (totalLoads === _bucketCount * _itemsCount) {
                    // already done
                    return;
                }
                if (!totalItemsTriggerred.hasOwnProperty(__item.getHash())) {
                    totalLoads += 1;
                    totalItemsTriggerred[__item.getHash()] = __item;
                    if (onDone && totalLoads === _bucketCount * _itemsCount) {
                        onDone();
                        onDone = undefined;
                    }
                }
            }

            beforeAll(function(__done) {
                var _items = 0;
                var _item;

                onDone = __done;

                //set up masters
                $Config.init(_confMaster);

                //fill locked bucket
                _lockedBucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket: (locked)');
                _unlockedBucket = $Bucket($Config($Config.TYPE.VIDEO, _bucketConfig), 'Test Bucket: (not locked)');
                _lockedBucket.lock();


                while (_items < _itemsCount * _bucketCount) {
                    _item = makeItem();
                    _item.addListener('loaded', onImageLoaded);
                    _item.addListener('deconstructed', onImageUnloaded);
                    ++_items;
                    if (_items % 2 === 0) {
                        _unlockedBucket.addItem(_item);
                    } else {
                        _lockedBucket.addItem(_item);
                    }
                }

                master = $Config.TYPE.VIDEO.pool;
            });

            afterAll(function() {
                //teardown masters
                $Config.deinit();
                totalLoads = 0;
                totalItemsTriggerred.length = 0;
                _lockedBucket = null;
                _unlockedBucket = null;
                master = null;
            });

            it('Should have master clear its content to stay beneath or equal to the max capacity (<=' + _confMaster.VIDEO.capacity + 'KB)', function() {
                expect(master.getMemorySize()).toBeLessThanOrEqual(_confMaster.VIDEO.capacity);
            });

            it('Should have master clear its content and have less items than initially cached (<' + _itemsCount * _bucketCount + ')', function() {
                expect(master.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
            });

            it('Should have master clear its content but have some items cached', function() {
                expect(master.getLinkedItems().length).toBeGreaterThan(0);
            });

            it('Should have SYSTEM master NOT clear its content (' + _itemsCount * _bucketCount + ')', function() {
                expect($Config.TYPE.SYSTEM.pool.getLength()).toEqual(_itemsCount * _bucketCount);
            });

            it('Should have total bucket linked memory less than or equal to max master capacity (<=' + _confMaster.VIDEO.capacity + 'KB)', function() {
                expect(master.getLinkedSize()).toBeLessThanOrEqual(master.getMemorySize());
            });

            it('Should have a total memory of (unlocked) bucket be equal 0', function() {
                expect(_unlockedBucket.getMemorySize()).toBeCloseTo(0);
            });

            it('Should have a total memory of (locked) bucket be equal master memory', function() {
                expect(_lockedBucket.getMemorySize()).toBeCloseTo(master.getMemorySize());
            });

            it('Should have a total length of (unlocked) bucket be equal 0', function() {
                expect(_unlockedBucket.getLength()).toEqual(0);
            });

            it('Should have a total length of (locked) bucket be equal master memory', function() {
                expect(_lockedBucket.getLength()).toEqual(master.getLength());
            });

            it('Should have total bucket linked memory be equal master total size', function() {
                expect(master.getLinkedSize()).toBeCloseTo(master.getMemorySize());
            });
        });
    });
});
