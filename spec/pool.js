/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error'
], function($Pool,
    $Config,
    $Eventful,
    $classify,
    $error) {
    'use strict';
    //$error.__DEBUG__ = true;

    function makePool() {
        return $Pool.bind(null, $Config($Config.TYPE.SYSTEM, {
            capacity: 100
        }), 'Test');
    }

    describe('Pool (Master)', function() {
        //
        describe('General', function() {
            //
            it('Should exist', function() {
                expect($Pool).toBeDefined();
            });

            it('Must be classified', function() {
                expect($classify.isClass($Pool)).toBe(true);
            });

            it('Must be subclassed from Eventful', function() {
                expect($Eventful.emitter.superof.indexOf($Pool) >= 0).toBe(true);
                //or
                expect($Pool.super === $Eventful.emitter).toBe(true);
            });

            it('Should throw if master pool was not configured', function() {
                expect(makePool).toThrow();
            });

            it('Should not throw if master pool was configured', function() {
                $Config.init($Config.DEFAULT_CONFIG);
                expect(makePool).not.toThrow();
                $Config.deinit();
            });
        });

        describe('Instance composition - config', function() {
            function make(__arg) {
                return $Pool.bind(null, __arg);
            }
            beforeEach(function() {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
            });
            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });
            it('Should not throw if proper config provided', function() {
                expect(makePool()).not.toThrow();
            });
            it('Should throw if null|undefined', function() {
                expect(make()).toThrow();
                expect(make(null)).toThrow();
                expect(make(undefined)).toThrow();
            });
            it('Should throw if Boolean', function() {
                expect(make(true)).toThrow();
                expect(make(false)).toThrow();
            });

            it('Should throw if Number|Float|Infinity (+-)', function() {
                expect(make(1)).toThrow();
                expect(make(0)).toThrow();
                expect(make(-1)).toThrow();
                expect(make(1.1)).toThrow();
                expect(make(-1.1)).toThrow();
                expect(make(Infinity)).toThrow();
                expect(make(-Infinity)).toThrow();
            });

            it('Should throw if Object|Array|Function|String (empty|none defined types)', function() {
                expect(make({})).toThrow();
                expect(make([])).toThrow();
                expect(make(['SYSTEM'])).toThrow();
                expect(make({
                    TYPE: 'SYSTEM'
                })).toThrow();
                expect(make(make)).toThrow();
                expect(make('make')).toThrow();
                expect(make('')).toThrow();
            });
        });

        describe('Instance composition - name', function() {
            function make(__arg) {
                return $Pool.bind(null, $Config($Config.TYPE.SYSTEM, {
                    capacity: 100
                }), __arg);
            }
            beforeEach(function() {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
            });
            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });
            it('Should not throw if proper name provided', function() {
                expect(make('Test')).not.toThrow();
            });
            it('Should not throw if null|undefined', function() {
                expect(make()).not.toThrow();
                expect(make(null)).not.toThrow();
                expect(make(undefined)).not.toThrow();
            });
            it('Should throw if Boolean', function() {
                expect(make(true)).toThrow();
                expect(make(false)).toThrow();
            });

            it('Should throw if Number|Float|Infinity (+-)', function() {
                expect(make(1)).toThrow();
                expect(make(0)).toThrow();
                expect(make(-1)).toThrow();
                expect(make(1.1)).toThrow();
                expect(make(-1.1)).toThrow();
                expect(make(Infinity)).toThrow();
                expect(make(-Infinity)).toThrow();
            });

            it('Should throw if Object|Array|Function|String (empty|none defined types)', function() {
                expect(make({})).toThrow();
                expect(make([])).toThrow();
                expect(make(['SYSTEM'])).toThrow();
                expect(make({
                    TYPE: 'SYSTEM'
                })).toThrow();
                expect(make(make)).toThrow();
                expect(make('')).toThrow();
            });
        });
    });
});
