/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/image',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error'
], function($Pool,
    $Image,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $error) {
    'use strict';

    var massTestCount = 100;
    //$error.__DEBUG__ = true;

    function near(__v, __to) {
        return Math.abs(__v - __to) < 1.0e-6;
    }

    describe('Pool clearOld Image implementation', function() {
        //
        function makeNewItem(__lifespan) {
            return $Image($Image.DEFAULT_URL + '?' + Math.random(), {
                lifespan: __lifespan
            });
        }

        describe('Pool clearOld lifespan of 1 ms implementation for 1 item', function() {
            var lifespan = 1;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if no arguments provided', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 of no arguments proveded', function(done) {
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if no arguments provided', function(done) {
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if lifespan is valid valid', function(done) {
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 0 if lifespan larger that of item', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if lifespan larger that of item', function(done) {
                expect(clear(lifespan)().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked and no args provided ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and no args provided', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked and no args provided', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and valid lifespan provided ', function(done) {
                _item.lock();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and valid lifespan provided', function(done) {
                _item.lock();
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked and valid lifespan provided', function(done) {
                _item.lock();
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });



            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(null, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(null, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getMemorySize()).toEqual(0);
                done();
            });
        });


        describe('Pool clearOld implementation of 10,000 ms lifespan (NOT expired), for 1 item', function() {
            var lifespan = 10000;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });


            it('Should have length of 1 if not older', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if not older', function(done) {
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
        });

        describe('Pool clearOld implementation ' + massTestCount + ' expired items (1 ms lifespan)', function() {
            var lifespan = 1;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });


            it('Should have length of 0', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(near(clear(lifespan)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan, true)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(null, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(null, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });

        describe('Pool clearOld implementation ' + massTestCount + ' not expired items (10,000 ms lifespan)', function() {
            var lifespan = 10000;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });



            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(null, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(null, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });
    });

    describe('Pool clearOld Texture implementation', function() {
        //
        function makeNewItem(__lifespan) {
            return $Texture($Texture.DEFAULT_TEXTURE.url + '?' + Math.random(), {
                lifespan: __lifespan
            });
        }

        //override default config to account for VIDEO/SYSTEM MEMORY
        var _conf = {
            SYSTEM: {
                capacity: 100000
            },
            VIDEO: {
                capacity: 100000
            },
            HEAP: {
                capacity: 100000
            }
        };

        describe('Pool clearOld lifespan of 1 ms implementation for 1 item', function() {
            var lifespan = 1;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if no arguments provided', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 of no arguments proveded', function(done) {
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if no arguments provided', function(done) {
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if lifespan is valid valid', function(done) {
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 0 if lifespan larger that of item', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if lifespan larger that of item', function(done) {
                expect(clear(lifespan)().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked and no args provided ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and no args provided', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked and no args provided', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and valid lifespan provided ', function(done) {
                _item.lock();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and valid lifespan provided', function(done) {
                _item.lock();
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked and valid lifespan provided', function(done) {
                _item.lock();
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });



            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(null, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(null, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getMemorySize()).toEqual(0);
                done();
            });
        });


        describe('Pool clearOld implementation of 10,000 ms lifespan (NOT expired), for 1 item', function() {
            var lifespan = 10000;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });


            it('Should have length of 1 if not older', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if not older', function(done) {
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear(lifespan)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear(lifespan)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced and no lifespan provided', function(done) {
                _item.lock();
                expect(clear(null, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(lifespan, true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
        });

        describe('Pool clearOld implementation ' + massTestCount + ' expired items (1 ms lifespan)', function() {
            var lifespan = 1;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });


            it('Should have length of 0', function(done) {
                expect(clear(lifespan)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(near(clear(lifespan)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan, true)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(null, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(null, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });

        describe('Pool clearOld implementation ' + massTestCount + ' not expired items (10,000 ms lifespan)', function() {
            var lifespan = 10000;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem();
            }

            function clear(__lifespan, __force) {
                return _pool.clearOld.bind(_pool, __lifespan, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });



            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(lifespan)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(null, true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(null, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(null, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(lifespan, true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(lifespan, true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });
    });
});
