/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/image',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error',
    'p!-logger'
], function($Pool,
    $Image,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $error,
    $logger) {
    'use strict';

    // $logger.setLevel($logger.loggingLevels.none);
    //$error.__DEBUG__ = true;

    describe('Pool capacity overflow - Image', function() {
        function makeItem() {
            return $Image($Image.DEFAULT_URL + '?' + Math.random());
        }
        describe('Hangling defaul LRU', function() {
            var config = {
                capacity: 100 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should default to LRU while removing itmes', function(done) {
                expect(_pool.__clearLRUTime).toBeGreaterThan(0);
                done();
            });

            it('Should not use FIFO|LFU', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                done();
            });
        });

        describe('Hangling while items are locked', function() {
            var config = {
                capacity: 100 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _image.lock();
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should default to LRU while removing itmes', function(done) {
                expect(_pool.__clearLRUTime).toBeGreaterThan(0);
                done();
            });

            it('Should not use FIFO|LFU', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                done();
            });
        });

        describe('Event hangling with custom logic', function() {
            var config = {
                capacity: 100 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;
            var onOverflownIsCalled = false;
            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    // _image.lock();
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function onOverflown(__pool) {
                __pool.getArray().map(function(v, i) {
                    if (i > 5) {
                        v.destroy();
                    }
                });
                onOverflownIsCalled = true;
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, config), 'Test Pool');
                _pool.addListener('overflown', onOverflown);
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
                onOverflownIsCalled = false;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should never fall into default scan', function() {
                expect(_pool.__scanTime).not.toBeDefined();
            });

            it('Should call listener of overflown event', function(done) {
                expect(onOverflownIsCalled).toEqual(true);
                done();
            });

            it('Should not use FIFO|LFU|LRU|EXP|OLD', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();

                expect(_pool.__clearExpiredTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();

                done();
            });
        });

        describe('FIFO hangling', function() {
            var config = {
                capacity: 100,
                fifo: true //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should have been scanned', function() {
                expect(_pool.__scanTime).toBeDefined();
            });

            it('Should fall into FIFO scan and scan exp|old', function() {
                expect(_pool.__clearFIFOTime).toBeDefined();
                expect(_pool.__croneFreeTime).toBeDefined();
            });

            it('Should not use LFU|LRU|OLD (no lifetimes specified)', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();
                done();
            });
        });

        describe('LFU hangling', function() {
            var config = {
                capacity: 100,
                lfu: true //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }


            beforeAll(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should have been scanned', function() {
                expect(_pool.__scanTime).toBeDefined();
            });

            it('Should fall into LFU scan and scan exp|old', function() {
                expect(_pool.__clearLFUTime).toBeDefined();
                expect(_pool.__croneFreeTime).toBeDefined();
            });

            it('Should not use LFU|LRU|OLD (no lifetimes specified)', function(done) {
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();
                done();
            });
        });
    });



    //TEXTURE TESTS



    describe('Pool capacity overflow - Texture', function() {
        //override default config to account for VIDEO/SYSTEM MEMORY
        var _conf = {
            SYSTEM: {
                capacity: 100000
            },
            VIDEO: {
                capacity: 100000
            },
            HEAP: {
                capacity: 100000
            }
        };

        function makeItem() {
            return $Texture($Texture.DEFAULT_URL + '?' + Math.random());
        }

        describe('Hangling defaul LRU', function() {
            var config = {
                capacity: 200 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (onDone && totalLoads === count) {
                    totalLoads = 0;
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (onDone && totalLoads === count) {
                    totalLoads = 0;
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should default to LRU while removing itmes', function(done) {
                expect(_pool.__clearLRUTime).toBeGreaterThan(0);
                done();
            });

            it('Should not use FIFO|LFU', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                done();
            });
        });
        describe('Hangling while items are locked', function() {
            var config = {
                capacity: 200 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _image.lock();
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should default to LRU while removing itmes', function(done) {
                expect(_pool.__clearLRUTime).toBeGreaterThan(0);
                done();
            });

            it('Should not use FIFO|LFU', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                done();
            });
        });

        describe('Event hangling with custom logic', function() {
            var config = {
                capacity: 400 //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;
            var onOverflownIsCalled = false;
            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    // _image.lock();
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function onOverflown(__pool) {
                __pool.getArray().map(function(v, i) {
                    if (i > 2) {
                        v.destroy();
                    }
                });
                onOverflownIsCalled = true;
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, config), 'Test Pool');
                _pool.addListener('overflown', onOverflown);
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
                onOverflownIsCalled = false;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should never fall into default scan', function() {
                expect(_pool.__scanTime).not.toBeDefined();
            });

            it('Should call listener of overflown event', function(done) {
                expect(onOverflownIsCalled).toEqual(true);
                done();
            });

            it('Should not use FIFO|LFU|LRU|EXP|OLD', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();

                expect(_pool.__clearExpiredTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();

                done();
            });
        });

        describe('FIFO hangling', function() {
            var config = {
                capacity: 200,
                fifo: true //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            beforeAll(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should have been scanned', function() {
                expect(_pool.__scanTime).toBeDefined();
            });

            it('Should fall into FIFO scan and scan exp|old', function() {
                expect(_pool.__clearFIFOTime).toBeDefined();
                expect(_pool.__croneFreeTime).toBeDefined();
            });

            it('Should not use LFU|LRU|OLD (no lifetimes specified)', function(done) {
                expect(_pool.__clearLFUTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();
                done();
            });
        });

        describe('LFU hangling', function() {
            var config = {
                capacity: 200,
                lfu: true //KB
            };
            var count = 50; //this would overflow the pool
            var _pool;
            var onDone;
            var totalLoads = 0;

            var totalItemsTriggerred = [];


            function onImageLoaded(__item) {
                totalLoads += 1;
                totalItemsTriggerred.push(__item);
                if (totalLoads === count) {
                    onDone();
                }
            }

            function onImageUnloaded(__item) {
                if (totalItemsTriggerred.indexOf(__item) < 0) {
                    totalLoads += 1;
                }
                if (totalLoads === count) {
                    onDone();
                }
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < count; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', onImageLoaded);
                    _image.addListener('deconstructed', onImageUnloaded);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }


            beforeAll(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, config), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterAll(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should remove enough items to be below the capacity', function(done) {
                expect(_pool.getMemorySize()).toBeGreaterThan(0);
                expect(_pool.getMemorySize()).toBeLessThan(config.capacity);
                done();
            });

            it('Should have been scanned', function() {
                expect(_pool.__scanTime).toBeDefined();
            });

            it('Should fall into LFU scan and scan exp|old', function() {
                expect(_pool.__clearLFUTime).toBeDefined();
                expect(_pool.__croneFreeTime).toBeDefined();
            });

            it('Should not use LFU|LRU|OLD (no lifetimes specified)', function(done) {
                expect(_pool.__clearFIFOTime).not.toBeDefined();
                expect(_pool.__clearLRUTime).not.toBeDefined();
                expect(_pool.__clearOldTime).not.toBeDefined();
                done();
            });
        });
    });
});
