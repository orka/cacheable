/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool', 'lib/image', 'lib/item', 'lib/config', 'p!-eventful', 'p!-defineclass', 'p!-error'], function($Pool, $Image, $Item, $Config, $Eventful, $classify, $error) {
    'use strict';
    var _pool;

    //$error.__DEBUG__ = true;

    function removeItem(__item, __force, __alreadyDestroyed) {
        return _pool.removeItem.bind(_pool, __item, __force, __alreadyDestroyed);
    }

    function addItem(__item, __override, __bypassUpdate) {
        return _pool.addItem.bind(_pool, __item, __override, __bypassUpdate);
    }

    function item(__url) {
        if (__url) {
            return $Image(__url);
        }
        return $Image($Image.DEFAULT_URL + '?' + Math.random());
    }

    function noneExtItem() {
        return $Item({}, $Image.DEFAULT_URL + '?' + Math.random());
    }

    describe('Pool (Master) removeItem() API', function() {
        beforeEach(function() {
            //set up masters
            $Config.init($Config.DEFAULT_CONFIG);
            _pool = $Pool($Config($Config.TYPE.SYSTEM, {
                capacity: 10000
            }), 'Test Pool');
            _pool.addItem(item());
        });
        afterEach(function() {
            //teerdown masters
            $Config.deinit();
        });

        it('Should throw if item is null|undefined', function() {
            expect(removeItem()).toThrow();
            expect(removeItem(null)).toThrow();
            expect(removeItem(undefined)).toThrow();
        });

        it('Should throw if item is Boolean', function() {
            expect(removeItem(true)).toThrow();
            expect(removeItem(false)).toThrow();
        });

        it('Should throw if item is a NUmber|Float|Infinity (+-)', function() {
            expect(removeItem(1)).toThrow();
            expect(removeItem(-1)).toThrow();
            expect(removeItem(0)).toThrow();
            expect(removeItem(-1.1)).toThrow();
            expect(removeItem(1.1)).toThrow();
            expect(removeItem(Infinity)).toThrow();
            expect(removeItem(-Infinity)).toThrow();
        });


        it('Should throw if item is Object|Array|Function', function() {
            expect(removeItem({
                s: 's'
            })).toThrow();
            expect(removeItem({})).toThrow();
            expect(removeItem([])).toThrow();
            expect(removeItem(item)).toThrow();
        });

        it('Should throw if item is not an instance of CacheItem (none typed)', function() {
            expect(removeItem(noneExtItem())).toThrow();
        });

        it('Should throw if not found', function() {
            expect(removeItem(item())).toThrow();
        });

        it('Should throw if found same hash but not equal to provided item', function() {
            _pool.addItem(item($Image.DEFAULT_URL));
            expect(removeItem(item($Image.DEFAULT_URL))).toThrow();
        });

        it('Should not throw if removed item', function() {
            expect(removeItem(_pool.getArray()[0])).not.toThrow();
        });

        it('Should return true if removed', function() {
            expect(removeItem(_pool.getArray()[0])()).toEqual(true);
        });

        it('Should update length', function() {
            removeItem(_pool.getArray()[0])();
            expect(_pool.getLength()).toEqual(0);
        });

        it('Should update Memory', function() {
            removeItem(_pool.getArray()[0])();
            expect(_pool.getMemorySize()).toEqual(0);
        });
    });
});
