/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass'
], function($Pool,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify) {
    'use strict';
    $Texture.__DEBUG__ = false;

    describe('Pool cache managment (update|clear|add)', function() {
        //
        describe('Single Item lifespan:undefined, lock:undefined', function() {
            var instance;
            var name = 'test';
            var texture;

            //Init Pool onfiguration
            //create cache Item
            //insert texture
            //test pool APIs
            beforeEach(function(done) {
                var _confinstance;
                $Config.init($Config.DEFAULT_CONFIG);
                _confinstance = $Config($Config.TYPE.VIDEO, $Config.DEFAULT_CONFIG.VIDEO);
                instance = $Pool(_confinstance, name);
                texture = $Texture($Texture.DEFAULT_TEXTURE.url + '?' + Math.random());
                texture.addListener('loaded', done);
                instance.addItem(texture);
            });

            afterEach(function() {
                $Config.deinit();
            });

            it('.getLength() return 1 ', function() {
                expect(instance.getLength()).toEqual(1);
            });

            it('.getMemorySize() equal texture size', function(done) {
                expect(instance.getMemorySize()).toBeGreaterThan(0);
                expect(instance.getMemorySize()).toEqual(texture.getMemorySize());
                done();
            });

            it('.clearAll() resets memory to 0', function(done) {
                expect(instance.clearAll().getMemorySize()).toEqual(0);
                done();
            });

            it('.clearAll() sets length to 0', function(done) {
                expect(instance.clearAll().getLength()).toEqual(0);
                done();
            });

            it('Texture.releaseData() sets Pool memory to 0', function(done) {
                texture.releaseData(); //should prompt the event being emitted by texture that updates pool
                expect(instance.getMemorySize()).toEqual(0);
                done();
            });

            it('Texture.releaseData() does not alter pool length', function(done) {
                texture.releaseData(); //should prompt the event being emitted by texture that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });
        });


        describe('Single Item lifespan:undefined, lock:true', function() {
            var instance;
            var name = 'test';
            var texture;
            var itemConfig = {
                locked: true,
                load: true
            };
            //Init Pool onfiguration
            //create cache Item
            //insert texture
            //test pool APIs
            beforeEach(function(done) {
                var _confinstance;
                $Config.init($Config.DEFAULT_CONFIG);
                _confinstance = $Config($Config.TYPE.VIDEO, $Config.DEFAULT_CONFIG.VIDEO);
                instance = $Pool(_confinstance, name);
                texture = $Texture($Texture.DEFAULT_TEXTURE.url + '?' + Math.random(), itemConfig);
                texture.addListener('loaded', done);
                instance.addItem(texture);
            });

            afterEach(function() {
                $Config.deinit();
            });

            it('.getLength() return 1 ', function() {
                expect(instance.getLength()).toEqual(1);
            });

            it('.getMemorySize() equal texture size', function(done) {
                expect(instance.getMemorySize()).toBeGreaterThan(0);
                expect(instance.getMemorySize()).toEqual(texture.getMemorySize());
                done();
            });

            it('.clearAll() does not resets memory to 0', function(done) {
                expect(instance.clearAll().getMemorySize()).toEqual(texture.getMemorySize());
                done();
            });

            it('.clearAll(true) resets memory to 0', function(done) {
                expect(instance.clearAll(true).getMemorySize()).toEqual(0);
                done();
            });

            it('.clearAll() does not sets length to 0', function(done) {
                expect(instance.clearAll().getLength()).toEqual(1);
                done();
            });

            it('.clearAll(true) does not sets length to 0', function(done) {
                expect(instance.clearAll(true).getLength()).toEqual(0);
                done();
            });

            it('Texture.releaseData() does not sets Pool memory to 0', function(done) {
                texture.releaseData(); //should prompt the event being emitted by texture that updates pool
                expect(instance.getMemorySize()).toEqual(texture.getMemorySize());
                done();
            });

            it('Texture.releaseData(true) does not sets Pool memory to 0', function(done) {
                texture.releaseData(true); //should prompt the event being emitted by texture that updates pool
                expect(instance.getMemorySize()).toEqual(0);
                done();
            });

            it('Texture.releaseData() does not alter pool length', function(done) {
                texture.releaseData(); //should prompt the event being emitted by texture that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });

            it('Texture.releaseData(true) does not alter pool length', function(done) {
                texture.releaseData(true); //should prompt the event being emitted by texture that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });
        });
    });
});
