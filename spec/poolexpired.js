/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/image',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error'
], function($Pool,
    $Image,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $error) {
    'use strict';

    var massTestCount = 100;
    //$error.__DEBUG__ = true;

    function near(__v, __to) {
        return Math.abs(__v - __to) < 1.0e-6;
    }

    describe('Pool Expired implementation', function() {
        function makeNewItem(__lifespan) {
            return $Image($Image.DEFAULT_URL + '?' + Math.random(), {
                lifespan: __lifespan
            });
        }

        describe('Pool Expired implementation of 1 ms lifespan (expired) for 1 item', function() {
            var lifespan = 1;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                expect(clear()().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(clear()().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(true)().getMemorySize()).toEqual(0);
                done();
            });
        });


        describe('of 10,000 ms lifespan (NOT expired), for 1 item', function() {
            var lifespan = 10000;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1', function(done) {
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image', function(done) {
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
        });

        describe(massTestCount + ' expired items (1 ms lifespan)', function() {
            var lifespan = 1;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                expect(clear()().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(near(clear()().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(true)().getMemorySize(), 0)).toEqual(true);
                done();
            });
        });

        describe(massTestCount + ' not expired items (10,000 ms lifespan)', function() {
            var lifespan = 10000;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });
    });


    //TEXTURE tests


    describe('Pool Expired implementation', function() {
        function makeNewItem(__lifespan) {
            return $Texture($Texture.DEFAULT_TEXTURE.url + '?' + Math.random(), {
                lifespan: __lifespan
            });
        }

        //override default config to account for VIDEO/SYSTEM MEMORY
        var _conf = {
            SYSTEM: {
                capacity: 100000
            },
            VIDEO: {
                capacity: 100000
            },
            HEAP: {
                capacity: 100000
            }
        };

        describe('Pool Expired implementation of 1 ms lifespan (expired) for 1 item', function() {
            var lifespan = 1;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                expect(clear()().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(clear()().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(true)().getMemorySize()).toEqual(0);
                done();
            });
        });

        describe('of 10,000 ms lifespan (NOT expired), for 1 item', function() {
            var lifespan = 10000;
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000
            };

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1', function(done) {
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image', function(done) {
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked', function(done) {
                _item.lock();
                expect(clear()().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked', function(done) {
                _item.lock();
                expect(clear()().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked and forced', function(done) {
                _item.lock();
                expect(clear(true)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of image if item locked and forced ', function(done) {
                _item.lock();
                expect(clear(true)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
        });

        describe(massTestCount + ' expired items (1 ms lifespan)', function() {
            var lifespan = 1;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                expect(clear()().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                expect(near(clear()().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of 0', function(done) {
                _pool.lockAll();
                expect(clear(true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0', function(done) {
                _pool.lockAll();
                expect(near(clear(true)().getMemorySize(), 0)).toEqual(true);
                done();
            });
        });

        describe(massTestCount + ' not expired items (10,000 ms lifespan)', function() {
            var lifespan = 10000;
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function makeItem() {
                return makeNewItem(lifespan);
            }

            function clear(__force) {
                return _pool.clearExpired.bind(_pool, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear()).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear()().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + '* item memory', function(done) {
                _pool.lockAll();
                expect(near(clear()().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(true)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount, function(done) {
                _pool.lockAll();
                expect(clear(true)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of ' + massTestCount + 'x items', function(done) {
                _pool.lockAll();
                expect(near(clear(true)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });
        });
    });
});
