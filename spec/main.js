this.$PROJECT_NAME = 'cacheable';

/**
 * Spec must be wrapped in a define function
 * then be followed by a window.onload() to trigger jasmine tests
 * window.onload() is a hack around jasmin not being triggerred when using requirejs
 * firt args array is the specs to be executed
 */
define([
    'jasmine-boot',
    'p!-logger',
    './item',
    './pool',
    './configMake',
    './configApi',
    './poolAddItem',
    './poolRemoveItem',
    './poolAddItemTexture',
    './poolTextureLoad',
    './poolImageLoad',
    './poolfifo',
    './poollfu',
    './poollru',
    './poolexpired',
    './olderthan',
    './pooloverflow',
    './bucket'
], function(
    $jasmine,
    $logger
) {
    'use strict';
    window.onload();
});
