 define(['lib/config',
     'p!-defineclass',
     'p!-error'
 ], function($Config,
     $classify,
     $error) {
     'use strict';
     // //$error.__DEBUG__ = true;

     describe('Config API ', function() {

         describe('For Options fifo: *', function() {
             //
             function make(__fifo) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: 1000,
                     fifo: __fifo
                 });
             }

             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should throw if Number|Float|Infinity (+-)', function() {
                 expect(make(1)).toThrow();
                 expect(make(-1)).toThrow();
                 expect(make(0)).toThrow();
                 expect(make(1.1)).toThrow();
                 expect(make(-1.1)).toThrow();
                 expect(make(Infinity)).toThrow();
                 expect(make(-Infinity)).toThrow();
             });

             it('Should throw if Object|Array|Function|String', function() {
                 expect(make([])).toThrow();
                 expect(make({})).toThrow();
                 expect(make([1, 2, 3])).toThrow();
                 expect(make({
                     s: 'make'
                 })).toThrow();
                 expect(make(make)).toThrow();
                 expect(make('')).toThrow();
                 expect(make('make')).toThrow();
             });

             it('Should have .fifo set to true if true', function() {
                 expect(make(true)().fifo).toEqual(true);
             });

             it('Should have .isFIFO() defined and return true', function() {
                 expect(make(true)().isFIFO).toBeDefined();
                 expect(make(true)().isFIFO()).toEqual(true);
             });

             it('Should have .fifo set to true if false (default if not lfu:true)', function() {
                 expect(make(false)().lru).toEqual(true);
                 expect(make(false)().isLRU()).toEqual(true);
             });
         });


         describe('For Options lfu: *', function() {
             //
             function make(__lfu) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: 1000,
                     lfu: __lfu
                 });
             }

             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should throw if Number|Float|Infinity (+-)', function() {
                 expect(make(1)).toThrow();
                 expect(make(-1)).toThrow();
                 expect(make(0)).toThrow();
                 expect(make(1.1)).toThrow();
                 expect(make(-1.1)).toThrow();
                 expect(make(Infinity)).toThrow();
                 expect(make(-Infinity)).toThrow();
             });

             it('Should throw if Object|Array|Function|String', function() {
                 expect(make([])).toThrow();
                 expect(make({})).toThrow();
                 expect(make([1, 2, 3])).toThrow();
                 expect(make({
                     s: 'make'
                 })).toThrow();
                 expect(make(make)).toThrow();
                 expect(make('')).toThrow();
                 expect(make('make')).toThrow();
             });


             it('Should have .lfu set to true if true', function() {
                 expect(make(true)().lfu).toEqual(true);
             });

             it('Should have .isFIFO() defined and return true', function() {
                 expect(make(true)().isLFU).toBeDefined();
                 expect(make(true)().isLFU()).toEqual(true);
             });
         });

         describe('For Options lfu: Boolean, fifo:Boolean', function() {
             function make(__f, __L) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: 1000,
                     lfu: __f,
                     fifo: __L
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should not throw when both false', function() {
                 expect(make(false, false)).not.toThrow();
             });

             it('Should have .lru set to true if lfo was set to false|nul|undefined (default)', function() {
                 var _instance = make(false, false)();
                 expect(_instance.isLRU()).toEqual(true);
                 expect(_instance.isLFU()).toEqual(false);
             });

             it('Should not throw when true | false', function() {
                 expect(make(true, false)).not.toThrow();
                 expect(make(false, true)).not.toThrow();
             });

             it('Should not throw when both true', function() {
                 expect(make(true, true)).not.toThrow();
             });

             it('Should set .lru to true if .fifo & .lfu set to false', function() {
                 var _instance = make(false, false)();
                 expect(_instance.isLRU()).toEqual(true);
                 expect(_instance.isLFU()).toEqual(false);
                 expect(_instance.isFIFO()).toEqual(false);
             });
         });

         describe('For Options lifespan: *', function() {
             function make(__lifespan) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: 1000,
                     lifespan: __lifespan
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should not throw if not a null|undefined', function() {
                 expect(make()).not.toThrow();
                 expect(make(null)).not.toThrow();
                 expect(make(undefined)).not.toThrow();
             });

             it('Should throw if Boolean', function() {
                 expect(make(true)).toThrow();
                 expect(make(false)).toThrow();
             });

             it('Should throw if Object|Array|Function', function() {
                 expect(make([])).toThrow();
                 expect(make({})).toThrow();
                 expect(make(make)).toThrow();
                 expect(make({
                     s: 's'
                 })).toThrow();
                 expect(make([1, 2, 3])).toThrow();
             });

             it('Should throw if String', function() {
                 expect(make('make')).toThrow();
                 expect(make('')).toThrow();
             });

             it('Should throw if not a positive Int', function() {
                 expect(make(-1)).toThrow();
                 expect(make(1.1)).toThrow();
                 expect(make(-1.1)).toThrow();
                 expect(make(Infinity)).toThrow();
                 expect(make(-Infinity)).toThrow();
                 expect(make(0)).toThrow();
             });

             it('Should not throw if positive Int |null|undefined', function() {
                 expect(make(100)).not.toThrow();
                 expect(make(null)).not.toThrow();
                 expect(make(undefined)).not.toThrow();
             });

             it('Should have .lifespan defined and equal param', function() {
                 expect(make(100)().lifespan).toBeDefined();
                 expect(make(100)().lifespan).toEqual(100);
             });

             it('Should set to null if not specified', function() {
                 expect(make()().lifespan).toEqual(null);
             });

             it('Should have .getLifespan() defined and equal param', function() {
                 expect(make(100)().getLifespan).toBeDefined();
                 expect(make(100)().getLifespan()).toEqual(100);
             });
         });

         describe('.getCapacity()', function() {
             function make(__capacity) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: __capacity
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should return a number', function() {
                 expect(make(1000)().getCapacity()).toEqual(1000);
             });
         });

         describe('.getUnits()', function() {
             function make(__capacity) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: __capacity
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should returns a number', function() {
                 expect(make(1000)().getUnits()).toEqual($Config.UNITS);
             });
         });

         describe('.isMasterType()', function() {
             function make(__capacity) {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: __capacity
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should return false if not master pool provided', function() {
                 expect(make(1000)().isMasterType()).toEqual(false);
                 expect(make(1000)().isMasterType(null)).toEqual(false);
                 expect(make(1000)().isMasterType(1)).toEqual(false);
                 expect(make(1000)().isMasterType(0)).toEqual(false);
                 expect(make(1000)().isMasterType([])).toEqual(false);
                 expect(make(1000)().isMasterType({})).toEqual(false);
                 expect(make(1000)().isMasterType(function() {})).toEqual(false);
             });

             it('Should return true if master pool provided', function() {
                 expect(make(1000)().isMasterType($Config.TYPE.SYSTEM.pool)).toEqual(true);
             });
         });

         describe('.getMemoryType()', function() {
             function make() {
                 return $Config.bind(null, $Config.TYPE.SYSTEM, {
                     capacity: 1000
                 });
             }
             beforeEach(function() {
                 $Config.init($Config.DEFAULT_CONFIG);
             });

             afterEach(function() {
                 $Config.deinit();
             });

             it('Should equal to specified type', function() {
                 expect(make()().getMemoryType()).toEqual($Config.TYPE.SYSTEM);
             });
         });
     });
 });
