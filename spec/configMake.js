/*
 * Testing Cache Item and its API's
 */
define(['lib/config',
    'p!-defineclass',
    'p!-error'
], function($Config,
    $classify,
    $error) {
    'use strict';
    // //$error.__DEBUG__ = true;

    describe('Config Make', function() {
        //
        describe('General', function() {
            //
            it('Should exist', function() {
                expect($Config).toBeDefined();
            });

            it('Must be classified', function() {
                expect($classify.isClass($Config)).toBe(true);
            });

            it('Must be have .TYPE (SYSTEM | VIDEO | HEAP) defined', function() {
                expect($Config.TYPE).toBeDefined();
                expect($Config.TYPE.SYSTEM).toBeDefined();
                expect($Config.TYPE.HEAP).toBeDefined();
                expect($Config.TYPE.VIDEO).toBeDefined();
            });

            it('Should throw when mutating .TYPE', function() {
                expect(function() {
                    $Config.TYPE.new = 'new';
                    $Config.TYPE.SYSTEM = 'override';
                    $Config.TYPE.VIDEO = 'override';
                    $Config.TYPE.HEAP = 'override';
                    $Config.TYPE = null;
                }).toThrow();
            });

            it('Should throw when mutating .DEFAULT_CONFIG', function() {
                expect(function() {
                    $Config.DEFAULT_CONFIG.new = 'new';
                    $Config.DEFAULT_CONFIG.SYSTEM = 'override';
                    $Config.DEFAULT_CONFIG.VIDEO = 'override';
                    $Config.DEFAULT_CONFIG.HEAP = 'override';
                    $Config.DEFAULT_CONFIG = null;
                }).toThrow();
            });

            it('Must be have .DEFAULT_CONFIG (SYSTEM | VIDEO | HEAP) defined', function() {
                expect($Config.DEFAULT_CONFIG).toBeDefined();
                expect($Config.DEFAULT_CONFIG.SYSTEM).toBeDefined();
                expect($Config.DEFAULT_CONFIG.HEAP).toBeDefined();
                expect($Config.DEFAULT_CONFIG.VIDEO).toBeDefined();
            });

            it('Must be have .init() defined', function() {
                expect($Config.init).toBeDefined();
            });

            it('Must be have .deinit() defined', function() {
                expect($Config.deinit).toBeDefined();
            });
        });

        describe('Master Cache Configuration $Config.init|.deinit', function() {
            function init(__arg) {
                return $Config.init.bind(null, __arg);
            }
            afterEach(function() {
                $Config.deinit();
            });
            //
            it('Should throw if null|undefined', function() {
                expect(init(null)).toThrow();
                expect(init(undefined)).toThrow();
                expect(init()).toThrow();
            });

            it('Should throw if Boolean', function() {
                expect(init(true)).toThrow();
                expect(init(false)).toThrow();
            });

            it('Should throw if function|EObject|Array|InvalidObject', function() {
                expect(init(init)).toThrow();
                expect(init({})).toThrow();
                expect(init([])).toThrow();
                expect(init({
                    someProp: true
                })).toThrow();
            });

            it('Should throw if Nimber|Float|Infinity (+-)', function() {
                expect(init(1)).toThrow();
                expect(init(0)).toThrow();
                expect(init(-1)).toThrow();
                expect(init(1.1)).toThrow();
                expect(init(-1.21)).toThrow();
                expect(init(Infinity)).toThrow();
                expect(init(-Infinity)).toThrow();
            });

            it('Should throw if empty string (__)', function() {
                expect(init('')).toThrow();
                expect(init('not empty')).toThrow();
            });

            it('Should throw if type matched but .capacity is not defined', function() {
                var _obj = {};
                _obj[$Config.TYPE.SYSTEM.name] = {};
                expect(init(_obj)).toThrow();
            });

            it('Should not throw if valid config provided', function() {
                expect(init($Config.DEFAULT_CONFIG)).not.toThrow();
            });

            it('Should throw if configured more than once without a teardown', function() {
                expect(init($Config.DEFAULT_CONFIG)).not.toThrow();
                expect(init($Config.DEFAULT_CONFIG)).toThrow();
            });

            it('Should not throw if configured again after teardown', function() {
                expect(init($Config.DEFAULT_CONFIG)).not.toThrow();
                $Config.deinit();
                expect(init($Config.DEFAULT_CONFIG)).not.toThrow();
            });
        });

        //this is instance of config tests
        describe('Intance composition', function() {
            //helper function
            function make(__arg, __arg2) {
                return $Config.bind(null, __arg, __arg2);
            }

            it('Cannot be created if master pool is not configured', function() {
                expect(make($Config.TYPE.SYSTEM, $Config.TYPE.SYSTEM)).toThrow();
            });

            it('Can be created if master pool is configured', function() {
                $Config.init($Config.DEFAULT_CONFIG);
                expect(make($Config.TYPE.SYSTEM, {
                    capacity: 666
                })).not.toThrow();
                $Config.deinit();
            });

            describe('Type param (first arg)', function() {
                var _options = {
                    capacity: 666
                };

                //helper funciton
                function make(__arg) {
                    return $Config.bind(null, __arg, _options);
                }

                beforeEach(function() {
                    $Config.init($Config.DEFAULT_CONFIG);
                });

                afterEach(function() {
                    $Config.deinit();
                });

                it('Should not throw if valid type provided', function() {
                    expect(make($Config.TYPE.SYSTEM)).not.toThrow();
                });

                it('Should throw if null|undefined', function() {
                    expect(make(null)).toThrow();
                    expect(make(undefined)).toThrow();
                    expect(make()).toThrow();
                });

                it('Should throw if Boolean', function() {
                    expect(make(true)).toThrow();
                    expect(make(false)).toThrow();
                });

                it('Should throw if function|EObject|Array|Invalid Object', function() {
                    expect(make(make)).toThrow();
                    expect(make({})).toThrow();
                    expect(make([])).toThrow();
                    expect(make({
                        someProp: true
                    })).toThrow();
                });

                it('Should throw if Nimber|Float|Infinity (+-)', function() {
                    expect(make(1)).toThrow();
                    expect(make(0)).toThrow();
                    expect(make(-1)).toThrow();
                    expect(make(1.1)).toThrow();
                    expect(make(-1.21)).toThrow();
                    expect(make(Infinity)).toThrow();
                    expect(make(-Infinity)).toThrow();
                });

                it('Should throw if empty string (__)', function() {
                    expect(make('')).toThrow();
                    expect(make('not empty')).toThrow();
                });
            });

            describe('Options param (second arg)', function() {
                var _options = {
                    capacity: 666
                };

                //helper funciton
                function make(__arg) {
                    return $Config.bind(null, $Config.TYPE.SYSTEM, __arg);
                }

                beforeEach(function() {
                    $Config.init($Config.DEFAULT_CONFIG);
                });

                afterEach(function() {
                    $Config.deinit();
                });

                it('Should not throw if valid type provided', function() {
                    expect(make(_options)).not.toThrow();
                });

                it('Should throw if null|undefined', function() {
                    expect(make(null)).toThrow();
                    expect(make(undefined)).toThrow();
                    expect(make()).toThrow();
                });

                it('Should throw if Boolean', function() {
                    expect(make(true)).toThrow();
                    expect(make(false)).toThrow();
                });

                it('Should throw if function|EObject|Array|Invalid Object', function() {
                    expect(make(make)).toThrow();
                    expect(make({})).toThrow();
                    expect(make([])).toThrow();
                    expect(make({
                        someProp: true
                    })).toThrow();
                });

                it('Should throw if Nimber|Float|Infinity (+-)', function() {
                    expect(make(1)).toThrow();
                    expect(make(0)).toThrow();
                    expect(make(-1)).toThrow();
                    expect(make(1.1)).toThrow();
                    expect(make(-1.21)).toThrow();
                    expect(make(Infinity)).toThrow();
                    expect(make(-Infinity)).toThrow();
                });

                it('Should throw if empty string (__)', function() {
                    expect(make('')).toThrow();
                    expect(make('not empty')).toThrow();
                });
            });

            describe('Deconstructor', function() {
                var _instance;
                beforeEach(function() {
                    $Config.init($Config.DEFAULT_CONFIG);
                    _instance = $Config($Config.TYPE.SYSTEM, {
                        capacity: 1000
                    });
                    _instance.deconstructor();
                });

                afterEach(function() {
                    $Config.deinit();
                });

                it('Should set .fifo. .lifespan .lfu .capacity .__type__ to null', function() {
                    expect(_instance.fifo).toEqual(null);
                    expect(_instance.lfu).toEqual(null);
                    expect(_instance.capacity).toEqual(null);
                    expect(_instance.__type__).toEqual(null);
                });
            });
        });
    });
});
