/*
 * Testing Cache Pool for Metadata
 */
define([
    'lib/pool',
    'lib/bucket',
    'lib/metadata',
    'lib/item',
    'lib/config'
], function(
    $Pool,
    $Bucket,
    $Metadata,
    $Item,
    $Config
) {
    'use strict';

    describe('Bucket - $Metadata implementation', function() {
        var _data = (function() {
            var _d = {};
            var _i;
            for (_i = 0; _i < 1000; ++_i) {
                _d[Math.random().toString()] = Math.random().toString();
            }
            return _d;
        })();
        var _dataSizeKB = JSON.stringify(_data).length / 1024;

        function makeItem() {
            return $Metadata({
                data: _data,
                hash: 'test' + Math.random()
            });
        }

        describe('add|remove 1 item', function() {
            var _master, _item, _bucket;

            var _bucketConfig = {
                capacity: _dataSizeKB + 1
            };

            beforeEach(function() {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);

                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket');

                _master = _bucket.getMaster();

                _item = makeItem();
                _bucket.addItem(_item);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should exist', function() {
                expect(_bucket).toBeDefined();
            });
            it('Should not throw when adding same item', function() {
                expect(_bucket.addItem.bind(_bucket, _item)).not.toThrow();
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have one item', function() {
                expect(_bucket.getLength()).toEqual(1);
            });

            it('Should have memory equal to item', function() {
                expect(_bucket.getMemorySize()).toEqual(_item.getMemorySize());
            });

            it('Should have added item to master', function() {
                expect(_master.getMemorySize()).toEqual(_bucket.getMemorySize());
                expect(_master.getLength()).toEqual(_bucket.getLength());
                expect(_master.getArray()[0]).toEqual(_bucket.getArray()[0]);
            });

            it('Should not throw when removed item', function() {
                expect(_bucket.removeItem.bind(_bucket, _item)).not.toThrow();
            });

            it('Should have length of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getLength()).toEqual(0);
            });

            it('Should have not removed item from master', function() {
                _bucket.removeItem(_item);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have memory of 0 when item removed', function() {
                expect(_bucket.removeItem(_item).getMemorySize()).toEqual(0);
            });

            it('Should find item after its been removed', function() {
                expect(_bucket.removeItem(_item).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should find item after its been removed with force', function() {
                expect(_bucket.removeItem(_item, true).findItemByHash(_item.getHash())).toEqual(_item);
            });

            it('Should not find in bucket if it was destroyed', function() {
                _item.destroy();
                expect(_bucket.getLength()).toEqual(0);
            });

            it('Should not find in master if it was destroyed', function() {
                _item.destroy();
                expect(_master.getLength()).toEqual(0);
            });

            it('Should be destroyed if Master was deconstructed', function() {
                $Config.deinit();
                expect(_bucket.__timeCreated).toEqual(null);
            });
        });

        describe('multiple instances relation (1 item in 1 bucket only - among many buckets)', function() {
            var _item;

            var _bucketConfig = {
                capacity: _dataSizeKB + 1
            };
            var _bucketsAmount = 100;
            var _buckets = [];

            beforeEach(function() {
                var _bucket, _i;
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket (' + _i + ')');
                    _buckets.push(_bucket);
                }
                //
                _item = makeItem();
                //only one bucket has item
                _bucket.addItem(_item);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
                _buckets = [];
            });

            it('Should have only one bucket the length of 1', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
            });

            it('Should link item to a bucket that exists in the master but does not in bucket when searching bucket', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    bucket.findItemByHash(_item.getHash());
                    //length should be updated if bucket had refferenced the item
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length);
            });
        });

        describe('multiple instances relation (same item in all buckets)', function() {
            var _master, _item;

            var _bucketConfig = {
                capacity: _dataSizeKB + 1
            };
            var _bucketsAmount = 100;
            var _buckets = [];

            beforeEach(function() {
                var _bucket, _i;
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _item = makeItem();

                for (_i = 0; _i < _bucketsAmount; _i += 1) {
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket (' + _i + ')');
                    _buckets.push(_bucket);
                    _bucket.addItem(_item);
                }
                //get holf of master
                _master = _bucket.getMaster();
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
                _buckets = [];
            });

            it('Should have length 1 in all buckets', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length);
            });

            it('Should have length of 1 in master', function() {
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in all buckets exept the one the iem was removed from', function() {
                var _totalCount = 0;
                _buckets.forEach(function(bucket, _i) {
                    if (_i === 0) {
                        bucket.removeItem(_item);
                    }
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_buckets.length - 1);
            });

            it('Should have length of 0 in all buckets and master when item is destroyed', function() {
                var _totalCount = 0;
                _item.destroy();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(0);
            });

            it('Should not be able to destroy item if any of the linked nodes is locked using item.destroy()', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.destroy();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(_totalCount);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should be able to destroy item if any of the linked nodes is locked using item.destroy(force)', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.destroy(true);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(0);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using buckets regardless if bucket is locked or not', function() {
                var _totalCount = 0;
                _buckets[50].lock();

                _buckets.forEach(function(bucket) {
                    bucket.removeItem(_item);
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using master (all none locked refference will be removed)', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _master.removeItem(_item);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 1 in locked buckets and master when item is removed using item', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.removeFromBuckets();

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(1);
                expect(_master.getLength()).toEqual(1);
            });

            it('Should have length of 0 in locked buckets and 1 in master when item is removed using item - force', function() {
                var _totalCount = 0;
                _buckets[50].lock();
                _item.removeFromBuckets(true);

                _buckets.forEach(function(bucket) {
                    _totalCount += bucket.getLength();
                });
                expect(_totalCount).toEqual(0);
                expect(_master.getLength()).toEqual(1);
            });
        });

        describe('config', function() {
            var _masterConfig = {
                SYSTEM: {
                    capacity: 100
                }
            };
            beforeEach(function() {
                //set up masters
                $Config.init(_masterConfig);
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should throw if bucket capacity is bigger than its master', function() {
                expect($Bucket.bind(null, $Config($Config.TYPE.SYSTEM, {
                    capacity: 101
                }))).toThrow();
            });

            it('Should throw if no config was specified', function() {
                expect($Bucket.bind(null)).toThrow();
            });
        });

        describe('If overflown: ', function() {
            var _bucketCount = 2;
            var _itemsCount = 5;

            var _masterConfig = {
                SYSTEM: {
                    capacity: _dataSizeKB * _bucketCount * _itemsCount - 1
                }
            };

            var _bucketConfig = {
                capacity: _masterConfig.SYSTEM.capacity
            };

            beforeEach(function() {
                var _bucket, _item, _i, _b;
                var _itemCollection = [];
                //set up masters
                $Config.init(_masterConfig);
                for (_i = 0; _i < _bucketCount; _i += 1) {
                    //
                    _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (' + _i + ')');
                    _itemCollection.length = 0;
                    for (_b = 0; _b < _itemsCount; _b += 1) {
                        _item = makeItem();
                        _itemCollection.push(_item);
                    }
                    //
                    _bucket.addArray(_itemCollection);
                }
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
            });

            it('Should have total bucket linked memory less than or equal to max master capacity', function() {
                expect($Config.TYPE.SYSTEM.pool.getLinkedSize()).toBeLessThanOrEqual($Config.TYPE.SYSTEM.pool.getMemorySize());
            });

            it('Should have master clear its content to stay beneath the max', function() {
                expect($Config.TYPE.SYSTEM.pool.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
                expect($Config.TYPE.SYSTEM.pool.getLinkedItems().length).toBeGreaterThan(0);
            });
        });

        describe('locked (one of) - If overflown: ', function() {
            var _bucketCount = 2;
            var _itemsCount = 10;

            var _masterConfig = {
                SYSTEM: {
                    capacity: _dataSizeKB * (_bucketCount * _itemsCount - 1)
                }
            };

            var _bucketConfig = {
                capacity: _masterConfig.SYSTEM.capacity
            };

            var master;


            beforeEach(function() {
                var _bucket, _item, _i;
                var _itemCollection = [];
                //set up masters
                $Config.init(_masterConfig);


                //fill locked bucket
                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (locked)');
                _bucket.lock();
                for (_i = 0; _i < _itemsCount; _i += 1) {
                    _item = makeItem();
                    _itemCollection.push(_item);
                }
                _bucket.addArray(_itemCollection);

                _itemCollection.length = 0;
                //fill locked bucket
                _bucket = $Bucket($Config($Config.TYPE.SYSTEM, _bucketConfig), 'Test Bucket: (not locked)');
                for (_i = 0; _i < _itemsCount; _i += 1) {
                    _item = makeItem();
                    _itemCollection.push(_item);
                }
                _bucket.addArray(_itemCollection);

                master = $Config.TYPE.SYSTEM.pool;
            });

            afterEach(function() {
                //teardown masters
                $Config.deinit();
                master = null;
            });

            it('Should have master clear its content to stay beneath the max', function() {
                expect(master.getLinkedItems().length).toBeLessThan(_itemsCount * _bucketCount);
                expect(master.getLinkedItems().length).toBeGreaterThan(0);
            });

            it('Should have total bucket linked memory less than or equal to max master capacity', function() {
                expect(master.getLinkedSize()).toBeLessThanOrEqual(master.getMemorySize());
            });

            it('Should have locked bucket retain its items over the non locked', function() {
                var _lockedItems = 0;
                var _unlockedItems = 0;
                master.getBuckets().forEach(function(__bucket) {
                    if (__bucket.isLocked()) {
                        _lockedItems += __bucket.getLinkedItems().length;
                    } else {
                        _unlockedItems += __bucket.getLinkedItems().length;
                    }
                });
                expect(_lockedItems).toBeGreaterThan(_unlockedItems);
            });

            it('Should have non locked bucket not retain its items over the locked bucket', function() {
                var _lockedItems = 0;
                var _unlockedItems = 0;
                master.getBuckets().forEach(function(__bucket) {
                    if (__bucket.isLocked()) {
                        _lockedItems += __bucket.getLinkedItems().length;
                    } else {
                        _unlockedItems += __bucket.getLinkedItems().length;
                    }
                });
                expect(_unlockedItems).not.toBeGreaterThan(_lockedItems);
            });
        });
    });
});
