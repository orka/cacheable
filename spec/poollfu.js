/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/image',
    'lib/texture',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error'
], function($Pool,
    $Image,
    $Texture,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $error) {
    'use strict';

    var massTestCount = 100;
    //$error.__DEBUG__ = true;

    function near(__v, __to) {
        return Math.abs(__v - __to) < 1.0e-6;
    }

    describe('Pool LFU implementation', function() {
        function makeItem() {
            return $Image($Image.DEFAULT_URL + '?' + Math.random());
        }
        describe('1 item', function() {
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000,
                fifo: true
            };

            function clear(__amount, __force) {
                return _pool.clearLFU.bind(_pool, __amount, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear(1)).not.toThrow();
                done();
            });

            it('Should have length of 0 (amount 1 pool length 1)', function(done) {
                expect(clear(1)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 (amount 1 pool length 1)', function(done) {
                expect(clear(1)().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear(1)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
            //LOCK + Force
            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(1, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1, true)().getMemorySize()).toEqual(0);
                done();
            });
        });


        describe(massTestCount + ' items', function() {
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function clear(__amount, __force) {
                return _pool.clearLFU.bind(_pool, __amount, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init($Config.DEFAULT_CONFIG);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.SYSTEM, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear(massTestCount)).not.toThrow();
                done();
            });

            it('Should have length of 0 (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                expect(clear(massTestCount)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                expect(near(clear(massTestCount)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount + ' if item locked (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of item if item locked (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(near(clear(massTestCount)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced (amount ' + massTestCount + 'pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(near(clear(massTestCount, true)().getMemorySize(), 0)).toEqual(true);
                done();
            });
        });
    });

    describe('Pool LFU implementation', function() {
        function makeItem() {
            return $Texture($Texture.DEFAULT_TEXTURE.url + '?' + Math.random());
        }
        //override default config to account for VIDEO/SYSTEM MEMORY
        var _conf = {
            SYSTEM: {
                capacity: 100000
            },
            VIDEO: {
                capacity: 100000
            },
            HEAP: {
                capacity: 100000
            }
        };


        describe('1 item', function() {
            var _pool, _item;
            var _poolConfig = {
                capacity: 1000,
                fifo: true
            };

            function clear(__amount, __force) {
                return _pool.clearLFU.bind(_pool, __amount, __force);
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                _item = makeItem();
                _item.addListener('loaded', done);
                _pool.addItem(_item);
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear(1)).not.toThrow();
                done();
            });

            it('Should have length of 0 (amount 1 pool length 1)', function(done) {
                expect(clear(1)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 (amount 1 pool length 1)', function(done) {
                expect(clear(1)().getMemorySize()).toEqual(0);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _item.lock();
                expect(clear(1)).not.toThrow();
                done();
            });

            it('Should have length of 1 if item locked (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1)().getLength()).toEqual(1);
                done();
            });

            it('Should have memory of item if item locked (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1)().getMemorySize()).toEqual(_item.getMemorySize());
                done();
            });
            //LOCK + Force
            it('Should not throw if item locked and forced', function(done) {
                _item.lock();
                expect(clear(1, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced (amount 1 pool length 1)', function(done) {
                _item.lock();
                expect(clear(1, true)().getMemorySize()).toEqual(0);
                done();
            });
        });


        describe(massTestCount + ' items', function() {
            var _pool;
            var _poolConfig = {
                capacity: 100000, //KB
                fifo: true
            };
            var onDone;
            var totalLoads = 0;

            function clear(__amount, __force) {
                return _pool.clearLFU.bind(_pool, __amount, __force);
            }

            function addItems() {
                var _i, _image, _items = [];
                for (_i = 0; _i < massTestCount; _i += 1) {
                    _image = makeItem();
                    _image.addListener('loaded', countloads);
                    _items.push(_image);
                }
                _pool.addArray(_items);
            }

            function countloads() {
                totalLoads += 1;

                if (totalLoads >= massTestCount) {
                    onDone();
                }
            }

            beforeEach(function(done) {
                //set up masters
                $Config.init(_conf);
                $Config($Config.TYPE.SYSTEM, _poolConfig);
                _pool = $Pool($Config($Config.TYPE.VIDEO, _poolConfig), 'Test Pool');
                addItems();
                onDone = done;
            });

            afterEach(function() {
                //teerdown masters
                $Config.deinit();
                totalLoads = 0;
            });

            it('Should not throw if amount argument valid', function(done) {
                expect(clear(massTestCount)).not.toThrow();
                done();
            });

            it('Should have length of 0 (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                expect(clear(massTestCount)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                expect(near(clear(massTestCount)().getMemorySize(), 0)).toEqual(true);
                done();
            });

            //LOCK

            it('Should not throw if item locked ', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount)).not.toThrow();
                done();
            });

            it('Should have length of ' + massTestCount + ' if item locked (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount)().getLength()).toEqual(massTestCount);
                done();
            });

            it('Should have memory of item if item locked (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(near(clear(massTestCount)().getMemorySize(), _pool.getArray()[0].getMemorySize() * massTestCount)).toEqual(true);
                done();
            });


            //LOCK + Force

            it('Should not throw if item locked and forced', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount, true)).not.toThrow();
                done();
            });

            it('Should have length of 0 if item locked and forced (amount ' + massTestCount + ' pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(clear(massTestCount, true)().getLength()).toEqual(0);
                done();
            });

            it('Should have memory of 0 if item locked and forced (amount ' + massTestCount + 'pool length ' + massTestCount + ')', function(done) {
                _pool.lockAll();
                expect(near(clear(massTestCount, true)().getMemorySize(), 0)).toEqual(true);
                done();
            });
        });
    });
});
