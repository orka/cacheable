/*
 * Testing Cache Item and its API's
 */
define(['lib/item', 'p!-eventful', 'p!-defineclass'], function($Item, $Eventful, $classify) {
    'use strict';
    var _type = {};

    function options() {
        return {
            lifespan: arguments[0],
            locked: arguments[1]
        };
    }
    describe('Item Class', function() {
        describe('General', function() {
            it('Should exist', function() {
                expect($Item).toBeDefined();
            });

            it('Must be classified', function() {
                expect($classify.isClass($Item)).toBe(true);
            });

            it('Must be subclassed from eventful', function() {
                expect($Eventful.emitter.superof.indexOf($Item) >= 0).toBe(true);
                //or
                expect($Item.super === $Eventful.emitter).toBe(true);
            });

            it('Must return instance of $Item', function() {
                expect($Item(_type, 123)).toBeDefined();
                expect($Item.isOffspring($Item(_type, 123))).toBe(true);
            });
        });

        describe('Instance composition HASH parameter', function() {
            it('Should throw if undefined', function() {
                expect($Item.bind(null, _type)).toThrow();
            });

            it('Should throw if null', function() {
                expect($Item.bind(null, _type, null)).toThrow();
            });

            it('Should throw if true', function() {
                expect($Item.bind(null, _type, true)).toThrow();
            });

            it('Should throw if false', function() {
                expect($Item.bind(null, _type, false)).toThrow();
            });

            it('Should throw if function', function() {
                expect($Item.bind(null, _type, function() {})).toThrow();
            });

            it('Should throw if Infinity (+-)', function() {
                expect($Item.bind(null, _type, Infinity)).toThrow();
                expect($Item.bind(null, _type, -Infinity)).toThrow();
            });

            it('Should throw if empty string (__)', function() {
                expect($Item.bind(null, _type, '')).toThrow();
                expect($Item.bind(null, _type, ' ')).toThrow();
            });

            it('Should NOT throw if valid Number|String', function() {
                expect($Item.bind(null, _type, 123)).not.toThrow();
                expect($Item.bind(null, _type, 'abc')).not.toThrow();
            });

            it('Should throw if array', function() {
                expect($Item.bind(null, _type, [])).toThrow();
            });

            it('Should throw if object', function() {
                expect($Item.bind(null, _type, {})).toThrow();
            });

            describe('API (hash)', function() {
                var hash = Math.random();
                var instance;
                beforeEach(function() {
                    instance = $Item(_type, hash);
                });

                it('Should have .__hash defined and be the same as param', function() {
                    expect(instance.__hash).toEqual(hash);
                });

                it('Should have .getHash() return param', function() {
                    expect(instance.getHash()).toEqual(hash);
                });

                it('Should have .isOfHash(__hash) test argument', function() {
                    expect(instance.isOfHash(132)).toEqual(false);
                    expect(instance.isOfHash(hash)).toEqual(true);
                });
            });
        });

        describe('Instance composition Optional Lifespan parameter', function() {
            it('Should not throw if undefined', function() {
                expect($Item.bind(null, _type, 123, options(null))).not.toThrow();
            });

            it('Should not throw if null', function() {
                expect($Item.bind(null, _type, 123, options(null))).not.toThrow();
            });

            it('Should not throw if Number [int]', function() {
                expect($Item.bind(null, _type, 123, options(132))).not.toThrow();
            });

            it('Should throw if Float', function() {
                expect($Item.bind(null, _type, 123, options(132.01))).toThrow();
            });

            it('Should not throw if negative Number [int]', function() {
                expect($Item.bind(null, _type, 123, options(-132))).toThrow();
            });

            it('Should throw if negative Float', function() {
                expect($Item.bind(null, _type, 123, options(-132.01))).toThrow();
            });

            it('Should throw if true', function() {
                expect($Item.bind(null, _type, 123, options(true))).toThrow();
            });

            it('Should throw if false', function() {
                expect($Item.bind(null, _type, 123, options(false))).toThrow();
            });

            it('Should throw if Infinity (+-)', function() {
                expect($Item.bind(null, _type, 123, options(Infinity))).toThrow();
                expect($Item.bind(null, _type, 123, options(-Infinity))).toThrow();
            });

            it('Should throw if String', function() {
                expect($Item.bind(null, _type, 123, options(''))).toThrow();
            });

            it('Should throw if Function', function() {
                expect($Item.bind(null, _type, 123, options(function() {}))).toThrow();
            });

            it('Should throw if array', function() {
                expect($Item.bind(null, _type, 123, options([]))).toThrow();
            });

            it('Should throw if object', function() {
                expect($Item.bind(null, _type, 123, options({}))).toThrow();
            });

            describe('API (Lifespan)', function() {
                var hash = Math.random();
                var lifespan = Math.round(Math.random() * 1000);
                var instance;
                beforeEach(function() {
                    instance = $Item(_type, hash, options(lifespan));
                });

                it('Should have .getLifespan() defined and be the same as param', function() {
                    expect(instance.getLifespan()).toEqual(lifespan);
                });

                it('Should have .getLifespan() return param', function() {
                    expect(instance.getLifespan()).toEqual(lifespan);
                });

                it('Should have .getLifespan() return a null|undefined if not defined', function() {
                    expect($Item(_type, hash).getLifespan()).toEqual(null);
                });
            });
        });

        describe('Instance composition Optional Lock parameter', function() {
            it('Should not throw if undefined', function() {
                expect($Item.bind(null, _type, 123, options(null))).not.toThrow();
            });

            it('Should not throw if null', function() {
                expect($Item.bind(null, _type, 123, options(null, null))).not.toThrow();
            });

            it('Should not throw if true', function() {
                expect($Item.bind(null, _type, 123, options(null, true))).not.toThrow();
            });

            it('Should not throw if false', function() {
                expect($Item.bind(null, _type, 123, options(null, false))).not.toThrow();
            });

            it('Should throw if Number [int|float]', function() {
                expect($Item.bind(null, _type, 123, options(null, 123))).toThrow();
                expect($Item.bind(null, _type, 123, options(null, 132.005))).toThrow();
            });

            it('Should throw if Infinity (+-)', function() {
                expect($Item.bind(null, _type, 123, options(null, Infinity))).toThrow();
                expect($Item.bind(null, _type, 123, options(null, -Infinity))).toThrow();
            });

            it('Should throw if String', function() {
                expect($Item.bind(null, _type, 123, options(null, ''))).toThrow();
            });

            it('Should throw if Function', function() {
                expect($Item.bind(null, _type, 123, options(null, function() {}))).toThrow();
            });

            it('Should throw if array', function() {
                expect($Item.bind(null, _type, 123, options(null, []))).toThrow();
            });

            it('Should throw if object', function() {
                expect($Item.bind(null, _type, 123, options(null, {}))).toThrow();
            });

            describe('API (Lock)', function() {
                var hash = Math.random();
                var lock = hash > 0.5;
                var instance;
                //
                beforeEach(function() {
                    instance = $Item(_type, hash, options(null, lock));
                });

                it('Should have .__locked__ defined and be the same as param', function() {
                    expect(instance.__locked__).toEqual(lock);
                });

                it('Should have .isLocked() return Boolean equal to .__locked__', function() {
                    expect(instance.isLocked()).toEqual(instance.__locked__);
                });

                it('Should have .lock() to have .isLocked() to true', function() {
                    instance.lock();
                    expect(instance.isLocked()).toEqual(true);
                });

                it('Should have .unlock() to have .isLocked() to false', function() {
                    instance.unlock();
                    expect(instance.isLocked()).toEqual(false);
                });
            });
        });

        describe('Misc API', function() {
            var hash = Math.random();
            var lock = hash > 0.5;
            var keepTiem = 5;
            var instance;
            //
            beforeEach(function() {
                instance = $Item(_type, hash, options(keepTiem, lock));
            });

            it('Should have .getLastUsedTime() return a number', function() {
                expect(instance.getLastUsedTime()).toEqual(jasmine.any(Number));
            });

            it('Should have .getTimesUsed() return 0,1,2 for every .update() is called', function() {
                expect(instance.getTimesUsed()).toEqual(0);
                instance.update();
                expect(instance.getTimesUsed()).toEqual(1);
                instance.update();
                expect(instance.getTimesUsed()).toEqual(2);
            });

            it('Should have .getTimeCreated() return a number', function() {
                expect(instance.getTimeCreated()).toEqual(jasmine.any(Number));
            });

            it('Should have .getMemorySize() throws for unwrapped class', function() {
                expect(instance.getMemorySize).toThrow();
            });

            it('Should have .getLifeTime() return a number <= 0', function() {
                expect(instance.getLifeTime()).toEqual(jasmine.any(Number));
            });

            it('Should have .isExpired() return Boolean', function() {
                expect(typeof instance.isExpired()).toEqual('boolean');
            });

            it('Should have .isExpired(1000) return false', function() {
                expect(instance.isExpired(1000)).toEqual(false);
            });

            it('Should have .isExpired() return false', function() {
                expect(instance.isExpired()).toEqual(false);
            });
        });
    });
});
