/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool', 'lib/image', 'lib/item', 'lib/config', 'p!-eventful', 'p!-defineclass'], function($Pool, $Image, $Item, $Config, $Eventful, $classify) {
    'use strict';
    $Image.__DEBUG__ = false;
    describe('Pool cache managment (update|clear|add)', function() {
        //
        describe('Single Item lifespan:undefined, lock:undefined', function() {
            var instance;
            var name = 'test';
            var image;

            //Init Pool onfiguration
            //create cache Item
            //insert image
            //test pool APIs
            beforeEach(function(done) {
                var _confinstance;
                $Config.init($Config.DEFAULT_CONFIG);
                _confinstance = $Config($Config.TYPE.SYSTEM, $Config.DEFAULT_CONFIG.SYSTEM);
                instance = $Pool(_confinstance, name);
                image = $Image($Image.DEFAULT_URL + '?' + Math.random());
                image.addListener('loaded', done);
                instance.addItem(image);
            });

            afterEach(function() {
                $Config.deinit();
            });

            it('.getLength() return 1 ', function() {
                expect(instance.getLength()).toEqual(1);
            });

            it('.getMemorySize() equal image size', function(done) {
                expect(instance.getMemorySize()).toBeGreaterThan(0);
                expect(instance.getMemorySize()).toEqual(image.getMemorySize());
                done();
            });

            it('.clearAll() resets memory to 0', function(done) {
                expect(instance.clearAll().getMemorySize()).toEqual(0);
                done();
            });

            it('.clearAll() sets length to 0', function(done) {
                expect(instance.clearAll().getLength()).toEqual(0);
                done();
            });

            it('Image.releaseData() sets Pool memory to 0', function(done) {
                image.releaseData(); //should prompt the event being emitted by image that updates pool
                expect(instance.getMemorySize()).toEqual(0);
                done();
            });

            it('Image.releaseData() does not alter pool length', function(done) {
                image.releaseData(); //should prompt the event being emitted by image that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });
        });


        describe('Single Item lifespan:undefined, lock:true', function() {
            var instance;
            var name = 'test';
            var image;
            var itemConfig = {
                locked: true,
                load: true
            };
            //Init Pool onfiguration
            //create cache Item
            //insert image
            //test pool APIs
            beforeEach(function(done) {
                var _confinstance;
                $Config.init($Config.DEFAULT_CONFIG);
                _confinstance = $Config($Config.TYPE.SYSTEM, $Config.DEFAULT_CONFIG.SYSTEM);
                instance = $Pool(_confinstance, name);
                image = $Image($Image.DEFAULT_URL + '?' + Math.random(), itemConfig);
                image.addListener('loaded', done);
                instance.addItem(image);
            });

            afterEach(function() {
                $Config.deinit();
            });

            it('.getLength() return 1 ', function() {
                expect(instance.getLength()).toEqual(1);
            });

            it('.getMemorySize() equal image size', function(done) {
                expect(instance.getMemorySize()).toBeGreaterThan(0);
                expect(instance.getMemorySize()).toEqual(image.getMemorySize());
                done();
            });

            it('.clearAll() does not resets memory to 0', function(done) {
                expect(instance.clearAll().getMemorySize()).toEqual(image.getMemorySize());
                done();
            });

            it('.clearAll(true) resets memory to 0', function(done) {
                expect(instance.clearAll(true).getMemorySize()).toEqual(0);
                done();
            });

            it('.clearAll() does not sets length to 0', function(done) {
                expect(instance.clearAll().getLength()).toEqual(1);
                done();
            });

            it('.clearAll(true) does not sets length to 0', function(done) {
                expect(instance.clearAll(true).getLength()).toEqual(0);
                done();
            });

            it('Image.releaseData() does not sets Pool memory to 0', function(done) {
                image.releaseData(); //should prompt the event being emitted by image that updates pool
                expect(instance.getMemorySize()).toEqual(image.getMemorySize());
                done();
            });

            it('Image.releaseData(true) does not sets Pool memory to 0', function(done) {
                image.releaseData(true); //should prompt the event being emitted by image that updates pool
                expect(instance.getMemorySize()).toEqual(0);
                done();
            });

            it('Image.releaseData() does not alter pool length', function(done) {
                image.releaseData(); //should prompt the event being emitted by image that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });

            it('Image.releaseData(true) does not alter pool length', function(done) {
                image.releaseData(true); //should prompt the event being emitted by image that updates pool
                expect(instance.getLength()).toEqual(1);
                done();
            });
        });
    });
});
