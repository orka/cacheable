/*
 * Testing Cache Pool and its API's
 */
define(['lib/pool',
    'lib/image',
    'lib/item',
    'lib/config',
    'p!-eventful',
    'p!-defineclass',
    'p!-error'
], function($Pool,
    $Image,
    $Item,
    $Config,
    $Eventful,
    $classify,
    $error) {
    'use strict';
    //
    var _pool;

    //$error.__DEBUG__ = true;

    function addItem(__item, __override, __bypassUpdate) {
        return _pool.addItem.bind(_pool, __item, __override, __bypassUpdate);
    }

    function addArray(__array, __override, __bypassUpdate) {
        return _pool.addArray.bind(_pool, __array, __override, __bypassUpdate);
    }

    function item(__url) {
        if (__url) {
            return $Image(__url);
        }
        return $Image($Image.DEFAULT_URL + '?' + Math.random());
    }

    function noneExtItem() {
        return $Item({}, $Image.DEFAULT_URL + '?' + Math.random());
    }


    describe('Pool (Master) addItem() API', function() {
        beforeEach(function() {
            //set up masters
            $Config.init($Config.DEFAULT_CONFIG);
            _pool = $Pool($Config($Config.TYPE.SYSTEM, {
                capacity: 10000
            }), 'Test Pool');
        });
        afterEach(function() {
            //teerdown masters
            $Config.deinit();
        });

        it('Should throw if item is null|undefined', function() {
            expect(addItem()).toThrow();
            expect(addItem(null)).toThrow();
            expect(addItem(undefined)).toThrow();
        });

        it('Should throw if item is Boolean', function() {
            expect(addItem(true)).toThrow();
            expect(addItem(false)).toThrow();
        });

        it('Should throw if item is a NUmber|Float|Infinity (+-)', function() {
            expect(addItem(1)).toThrow();
            expect(addItem(-1)).toThrow();
            expect(addItem(0)).toThrow();
            expect(addItem(-1.1)).toThrow();
            expect(addItem(1.1)).toThrow();
            expect(addItem(Infinity)).toThrow();
            expect(addItem(-Infinity)).toThrow();
        });


        it('Should throw if item is Object|Array|Function', function() {
            expect(addItem({
                s: 's'
            })).toThrow();
            expect(addItem({})).toThrow();
            expect(addItem([])).toThrow();
            expect(addItem(item)).toThrow();
        });

        it('Should throw if item is not an instance of CacheItem (none typed)', function() {
            expect(addItem(noneExtItem())).toThrow();
        });

        it('Should not throw if item is typed instance matching Pool type (SYSTEM|IMAGE)', function() {
            expect(addItem(item())).not.toThrow();
        });

        it('Should should have length 1', function() {
            expect(addItem(item())().getLength()).toEqual(1);
        });

        it('Should should have memory === item size', function() {
            expect(addItem(item())().getMemorySize()).toEqual(item().getMemorySize());
        });

        it('Should should not add the same item again', function() {
            var _item = item();
            addItem(_item)();
            expect(addItem(_item)().getLength()).toEqual(1);
            expect(addItem(_item)().getMemorySize()).toEqual(_item.getMemorySize());
        });

        it('Should should destroy same hash item with "override"', function() {
            var _item = item();
            var _itemOfTheSameHash = item(_item.url);

            addItem(_item)();
            addItem(_itemOfTheSameHash, true)();
            expect(_pool.getLength()).toEqual(1);
            expect(_item.__timeCreated).toEqual(null);
            expect(_pool.getMemorySize()).toEqual(_itemOfTheSameHash.getMemorySize());
        });

        it('Should NOT destroy or override same hash item with "override" false', function() {
            var _item = item();
            var _itemOfTheSameHash = item(_item.url);

            // spyOn(_item, '__destroy__');
            addItem(_item)();
            addItem(_itemOfTheSameHash)();
            expect(_pool.getLength()).toEqual(1);
            expect(_item.__timeCreated).not.toEqual(null);
            expect(_pool.getMemorySize()).toEqual(_item.getMemorySize());
        });

        it('Should not update if __bypassUpdate is true', function() {
            expect(addItem(item(), false, true)().__scanTime).not.toBeDefined();
        });

        it('Should not update if __bypassUpdate is not true and not overflown', function() {
            var _pool = addItem(item(), false)();
            expect(_pool.__scanTime).not.toBeDefined();
            expect(_pool.__isOverflown()).toEqual(false);
        });
    });

    describe('Pool (Master) addArray(3) API', function() {
        function makeDiff() {
            return [item(), item(), item()];
        }

        function makeSameURL() {
            var _item = item();
            return [item(_item.url), item(_item.url), _item];
        }

        function makeSameItems() {
            var _item = item();
            return [_item, _item, _item];
        }

        beforeEach(function() {
            //set up masters
            $Config.init($Config.DEFAULT_CONFIG);
            _pool = $Pool($Config($Config.TYPE.SYSTEM, {
                capacity: 10000
            }), 'Test Pool');
        });
        afterEach(function() {
            //teerdown masters
            $Config.deinit();
        });


        it('Should throw if item is null|undefined', function() {
            expect(addArray()).toThrow();
            expect(addArray(null)).toThrow();
            expect(addArray(undefined)).toThrow();
        });

        it('Should throw if item is Boolean', function() {
            expect(addArray(true)).toThrow();
            expect(addArray(false)).toThrow();
        });

        it('Should throw if item is a NUmber|Float|Infinity (+-)', function() {
            expect(addArray(1)).toThrow();
            expect(addArray(-1)).toThrow();
            expect(addArray(0)).toThrow();
            expect(addArray(-1.1)).toThrow();
            expect(addArray(1.1)).toThrow();
            expect(addArray(Infinity)).toThrow();
            expect(addArray(-Infinity)).toThrow();
        });


        it('Should throw if item is Object|Function', function() {
            expect(addArray({
                s: 's'
            })).toThrow();
            expect(addArray({})).toThrow();
            expect(addArray(item)).toThrow();
        });

        it('Should have length 3', function() {
            expect(addArray(makeDiff())().getLength()).toEqual(3);
        });

        it('Should have memory === item size *3', function() {
            expect(addArray(makeDiff())().getMemorySize()).toEqual(item().getMemorySize() * 3);
        });

        it('Should not add the same item again', function() {
            var _array = makeSameItems();
            expect(addArray(_array)().getMemorySize()).toEqual(item().getMemorySize());
            expect(addArray(_array)().getLength()).toEqual(1);
        });

        it('Should destroy same hash item with "override" true and update length accoringly', function() {
            var _array = makeSameURL();
            // spyOn(_item, '__destroy__');
            expect(addArray(_array, true)().getLength()).toEqual(1);
            expect(_array[0].__deconstructTime).toBeDefined(); //deconstructed
        });

        it('Should destroy same hash item with "override" true and update mem accoringly', function() {
            var _array = makeSameURL();
            // spyOn(_item, '__destroy__');
            expect(addArray(_array, true)().getMemorySize()).toEqual(item().getMemorySize());
            expect(_array[0].__deconstructTime).toBeDefined(); //deconstructed
        });

        it('Should NOT destroy or override same hash item with "override" false', function() {
            var _array = makeSameURL();

            // spyOn(_item, '__destroy__');
            expect(addArray(_array)().getLength()).toEqual(1);
            expect(addArray(_array)().getMemorySize()).toEqual(item().getMemorySize());
            expect(_array[0].__deconstructTime).not.toBeDefined(); //deconstructed
        });

        it('Should not update if __bypassUpdate is true', function() {
            expect(addArray(makeDiff(), false, true)().__scanTime).not.toBeDefined();
        });

        it('Should not update if __bypassUpdate is not true and not overflown', function() {
            var _pool = addArray(makeDiff(), false)();
            expect(_pool.__scanTime).not.toBeDefined();
            expect(_pool.__isOverflown()).toEqual(false);
        });
    });
});
