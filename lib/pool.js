/**
 * Pool is an abstract array intended to handle data as an associated array
 * manged be controller.js only
 */
define(['p!-cacheable/item',
    'p!-cacheable/error',
    'p!-eventful',
    'p!-assertful',
    'p!-math',
    'p!-logger'
], function($Item,
    $error,
    $eventful,
    $assertful,
    $math,
    $logger) {
    'use strict';
    var Pool;
    var THRESHHOLD = 0.8; // *100, the amount to be minimally cleared when pool is overflowing, except the case of .length being less or equal to it

    function oldestItemSort(__itemA, __itemB) {
        //if less than 0, sort A to a lower index than B, i.e. A comes first.
        // return !__itemA.isLinked() && __itemA.getTimeCreated() - __itemB.getTimeCreated() ? -1 : 1;
        return __itemA.getTimeCreated() - __itemB.getTimeCreated();
    }

    function LFUSort(__itemA, __itemB) {
        //if less than 0, sort A to a lower index than B, i.e. A comes first.
        // return !__itemA.isLinked() && __itemA.getTimesUsed() - __itemB.getTimesUsed() ? -1 : 1;
        return __itemA.getTimesUsed() - __itemB.getTimesUsed();
    }

    function LRUSort(__itemA, __itemB) {
        //if less than 0, sort A to a lower index than B, i.e. A comes first.
        // return !__itemA.isLinked() && __itemA.getLastUsedTime() - __itemB.getLastUsedTime() ? -1 : 1;
        return __itemA.getLastUsedTime() - __itemB.getLastUsedTime();
    }

    function lockFilter(__item) {
        return __item.isLocked();
    }

    function notLockedFilter(__item) {
        return !__item.isLocked();
    }

    function linkedFilter(__item) {
        return __item.isLinked();
    }

    function notLinkedFilter(__item) {
        return !__item.isLinked();
    }

    /**
     * Class API
     * @type {Object}
     */
    Pool = $eventful.emitter.subclass('CachePool', {

        /**
         * Main Class constructor
         * @param   {Object} __config pool configuration
         * @param   {String} __name Optional name of the pool instance
         */
        constructor: function(__config, __name) {
            Pool.super.constructor.call(this);

            if (!$assertful.object(__config) || !$assertful.func(__config.getCapacity) || !$assertful.positive(__config.getCapacity())) {
                throw $error($error.TYPE.TYPE_ERROR, ' constructor Config object is invalid', arguments);
            }

            if (!$assertful.optString(__name)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Invalid name', arguments);
            }

            this.config = __config;
            this.name = __name;
            this.__cache__ = {};
            this.length = 0;
            this.__memorySize__ = 0;
            this.__updateDuration__ = -1; //negative number indicates no update was ever performed

            //cache reference
            this.__copyOnItemDestroyed__ = this.__onItemDestroyed__.bind(this);
            this.__copyOnItemUpdate__ = this.__onItemUpdate__.bind(this);
            this.__copyOnError__ = this.__onError__.bind(this);
            this.__linkedBuckets__ = [];
            this.addListener('error', this.__copyOnError__);
        },

        /**
         * Clears up set variables and destroys pool items
         */
        deconstructor: function() {
            //notify all buckets the pool is being destroyed before the items cleared
            this.emit('destroyed');
            this.__clearAll__(true);
            this.__cache__ = null; // this will dereference the entire cache
            this.length = null;
            this.__clearExpiredTime = null;
            this.__clearFIFOTime = null;
            this.__clearLFUTime = null;
            this.__clearLRUTime = null;
            this.__clearAllTime = null;
            this.__lockAllTime = null;
            this.__updateDuration__ = null;
            this.__scanTime = null;
            this.__clearOldTime = null;
            this.__linkedBuckets__ = null;
            this.__croneFreeTime = null;
            Pool.super.deconstructor.call(this);
        },



        //-----------------------------------------     LOG     ---------------------------------------------



        /**
         * Customized log function using logger
         * @param   {String} __string Message
         * @returns {Object} this instance
         */

        verbose: function() {
            var _cargo = Array.prototype.slice.call(arguments, 0);
            //identify
            _cargo.unshift(Pool.name + '[' + this.config.getType().name + '] (' + this.getName() + ') ');
            _cargo.push($logger.TAGS.CACHE);
            $logger.verbose.apply($logger, _cargo);
            return this;
        },

        /**
         * Dumps all locked items to the console
         * @param {Boolean} __dataOnly if true logs the data of item, else item object itself
         * @returns {Object} this instance
         */
        logLocked: function(__dataOnly) {
            this.verbose('Begin Locked dump >>>>>>>>>>>>>>>>>>>>>>>');
            this.forEach(function(__item) {
                if (__item.isLocked()) {
                    this.verbose(__dataOnly ? __item.getData() : __item);
                }
            }.bind(this));
            this.verbose('End Locked dump >>>>>>>>>>>>>>>>>>>>>>>');
            return this;
        },

        /**
         * Dumps all not locked items to the console
         * @param {Boolean} __dataOnly if true logs the data of item, else item object itself
         * @returns {Object} this instance
         */
        logNotLocked: function(__dataOnly) {
            this.verbose('Begin Locked dump >>>>>>>>>>>>>>>>>>>>>>>');
            this.forEach(function(__item) {
                if (!__item.isLocked()) {
                    this.verbose(__dataOnly ? __item.getData() : __item);
                }
            }.bind(this));
            this.verbose('End Locked dump >>>>>>>>>>>>>>>>>>>>>>>');
            return this;
        },

        /**
         * Dumps all linked items to the console
         * @param {Boolean} __dataOnly if true logs the data of item, else item object itself
         * @returns {Object} this instance
         */
        logLinked: function(__dataOnly) {
            this.verbose('Begin Linked dump >>>>>>>>>>>>>>>>>>>>>>>');
            this.forEach(function(__item) {
                if (__item.isLinked()) {
                    this.verbose(__dataOnly ? __item.getData() : __item);
                }
            }.bind(this));
            this.verbose('End Linked dump >>>>>>>>>>>>>>>>>>>>>>>');
            return this;
        },

        /**
         * Dumps all non linked items to the console
         * @param {Boolean} __dataOnly if true logs the data of item, else item object itself
         * @returns {Object} this instance
         */
        logNotLinked: function(__dataOnly) {
            this.verbose('Begin Linked dump >>>>>>>>>>>>>>>>>>>>>>>');
            this.forEach(function(__item) {
                if (!__item.isLinked()) {
                    this.verbose(__dataOnly ? __item.getData() : __item);
                }
            }.bind(this));
            this.verbose('End Linked dump >>>>>>>>>>>>>>>>>>>>>>>');
            return this;
        },
        /**
         * Dumps all items to the console
         * @param {Boolean} __dataOnly if true logs the data of item, else item object itself
         * @returns {Object} this instance
         */
        logAll: function(__dataOnly) {
            this.verbose('Begin dump >>>>>>>>>>>>>>>>>>>>>>>');
            this.forEach(function(__item) {
                this.verbose(__dataOnly ? __item.getData() : __item);
            }.bind(this));
            this.verbose('End dump >>>>>>>>>>>>>>>>>>>>>>>');
            return this;
        },



        //-----------------------------------------     ADD     ---------------------------------------------



        /**
         * PRIVATE METHOD ONLY!
         * No checks done here, Adds new item and increment length and weight of pool
         * USE at your own risk!
         * @param {Object} __item cache item to be referenced
         * @param {Boolean} __bypassUpdate if true prevents the pool scan and clear actions
         */
        __refferenceItem__: function(__item, __bypassUpdate) {
            var _size = __item.getMemorySize(this.config.getType());
            __item.saveSize(_size);
            /* if (isNaN(_size) || _size <= 0) {
                 throw 'Unable to cache item as size could not be determined!';
             }*/
            //all cache item should register pools associated with it
            __item.addListener('destroyed', this.__copyOnItemDestroyed__);
            __item.addListener('sizeUpdate', this.__copyOnItemUpdate__);
            __item.addListener('error', this.__copyOnError__);
            //add new cache item
            this.__cache__[__item.getHash()] = __item;
            //add size
            this.__memorySize__ += _size;
            //update length
            this.length += 1;

            //call update only after item was added
            //at this point cache may be temporarily overflown, so need to update
            if (!__bypassUpdate) {
                this.__update__();
            }
        },

        /**
         * Optimized version of addItem
         * Bypasses all the safety checks
         * @param   {Object} __item     cache item
         * @param   {Boolean} __override if true overrides existing object - removing (destroying is Master) current item
         * @param {Boolean} __bypassUpdate if true prevents the pool scan and clear actions
         * @returns {Boolean} true if added
         */
        __addItem__: function(__item, __override, __bypassUpdate) {
            var _hash = __item.getHash();
            //test if cache already exists
            var _existingItem = this.__findItemByHash__(_hash);

            if (!_existingItem) {
                this.__refferenceItem__(__item, __bypassUpdate);
                this.verbose('hash "' + _hash + '" - added!');
                return true;
            }

            if (__item !== _existingItem && __override) {
                //if item already exists and override flag set - we'll destroy current item and set new on in its place

                //this will invalidate everything that was cached as cargo
                //add true to force the removal
                //this will remove the items from every pool its registered at
                //WARNING - node pools will not contain this item, however it will be cache and
                //available to all
                //only master pool
                _existingItem.__destroy__();
                this.__refferenceItem__(__item, __bypassUpdate);
                this.verbose('hash "' + _hash + '" - override done!');

                return true;
            }

            //if pool item with the same key found but no override flag given - just log
            this.verbose('hash "' + _hash + '" - already exists');

            return false;
        },

        /**
         * Adds new entry to the pool
         * Creates new PoolItem instance with __item as its cargo - if added
         * @param {Object} __item item instance
         * @param {Boolean} __override if true same key pool item will be destroyed and replaced be a new item
         * @param {Boolean} __bypassUpdate if true prevents the pool scan and clear actions
         * @returns {Object} this instance
         */
        addItem: function(__item, __override, __bypassUpdate) {
            //get hash of item
            var _hash;

            if (!$Item.isOffspring(__item) || !__item.instanceof($Item)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to cache Item! Item argument must be defined and an offspring of $Item class!', arguments));
                return this;
            }

            if (!$assertful.optBoolean(__override) || !$assertful.optBoolean(__bypassUpdate)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Invalid optional arguments. Use Boolean only!', arguments));
                return this;
            }

            if (__item.getType() !== this.config.getType()) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Item added is not of this cache type', arguments));
                return this;
            }

            _hash = __item.getHash();

            //test if hash is valid
            if (!$assertful.hash(_hash)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'unable to discern the "hash"', _hash));
                return this;
            }

            this.__addItem__(__item, __override, __bypassUpdate);

            //current number of cache items
            return this;
        },

        /**
         * Add all values of given array to the pool chase using .addItem method
         * @param {Array} __array collection of cacheables
         * @param {Boolean} __override Optional if true same key pool item will be destroyed and replaced be a new item
         * @param {Boolean} __bypassUpdate if true does not trigger pool scan action
         * @returns {Object} this instance
         */
        addArray: function(__array, __override, __bypassUpdate) {
            var _i;

            if (!$assertful.array(__array)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Provided argument is not an Array!', arguments));
                return this;
            }

            if (!$assertful.optBoolean(__override) || !$assertful.optBoolean(__bypassUpdate)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Invalid optional arguments. Use Boolean only!', arguments));
                return this;
            }

            for (_i = 0; _i < __array.length; _i += 1) {
                //bypass update by setting bypass to true
                this.addItem(__array[_i], __override, true);
            }

            //call update only after item was added
            //at this point cache may be temporarily overflown, so need to update
            if (__array.length > 0 && !__bypassUpdate) {
                this.__update__();
            }

            return this;
        },



        //-----------------------------------------     REMOVE     ---------------------------------------------



        /**
         * takes collection (kind of duplicates, but this collection is specially sorted)
         * If collection is not specified - unsorted array is used
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked aways
         * @param   {Array} __collection sorted collection of cache items
         * @returns {Number}              Amount removed
         */
        __removeAmount__: function(__amount, __force, __collection) {
            var _collection, _i, _item, _amountRemoved = 0;
            _collection = __collection || this.getArray();
            //loop over sorted array and destroy just the amount needed to be cleaned
            for (_i = 0; _i < _collection.length && _amountRemoved < __amount; _i += 1) {
                _item = _collection[_i]; // for debugging
                //call optimized method and iterate amount only when items were removed
                if (this.__removeItem__(_item, __force) === true) {
                    _amountRemoved += 1;
                }
            }
            return _amountRemoved;
        },

        /**
         * Event emitted by cache item when destroyed
         * Remove key (dereference to be picked up by JSGC) and reduce current weight
         * @param  {Object} __item Item instance
         * @param {Boolean} __force forces item removal if locked
         * @returns {Object} Boolean
         */
        removeItem: function(__item, __force) {
            var _hash, _foundItem;

            if (!$Item.isOffspring(__item) || !__item.instanceof($Item)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to remove item, wrong arguments provided', arguments));
                return false;
            }

            if (!$assertful.optBoolean(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use Boolean to set flags!', arguments));
                return false;
            }

            if (__item.getType() !== this.config.getType()) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to remove item of a foreign cache type!', arguments));
                return false;
            }

            //find item
            _hash = __item.getHash();
            _foundItem = this.__findItemByHash__(_hash, true);

            if (!_foundItem) {
                this.emit('error',
                    $error($error.TYPE.ITEM_NOT_FOUND,
                        'requested hash removal failed! Not found.', arguments)
                );
                return false;
            }

            if (_foundItem && _foundItem !== __item) {
                this.emit('error',
                    $error($error.TYPE.ITEM_MISSMATCH,
                        'Found item is not the same instance of supplied argument!', [__item, _foundItem]));
                return false;
            }

            return this.__removeItem__(__item, __force);
        },

        /**
         * Warning! - no argument validity checks are performed - use at your own risk!
         * Event emitted by cache item when destroyed
         * Remove key (dereference to be picked up by JSGC) and reduce current weight
         * @param  {Object} __item Item instance
         * @param {Boolean} __force forces item removal if locked
         * @param {Boolean} __destroyed if true - bypasses item destroy call - done to ensure function doesn't get called twice
         * @returns {Boolean} true if removed
         */
        __removeItem__: function(__item, __force, __destroyed) {
            var _size = __item.getMemorySize(this.config.getType());
            var _hash = __item.getHash();

            if (__force === true || __item.isSafeToDestroy()) {
                //if forced or not locked & not used by any nodes - dereference item and destroy it!
                //else destroy if not already destroyed
                this.__derefferenceItem__(_hash, __item, _size);

                if (!__destroyed) {
                    //destroy item removed from pool
                    //this will force users of detached cache items to pay attention
                    //to when cache is overflown or is manipulated by unchecked mechanisms
                    //This action will emit 'destroyed' event by the item itself, however, this pool has already
                    //removed related listener, so this pool will not be bothered again by this item!;
                    __item.__destroy__();
                }

                return true;
            }

            if (__force !== true) {
                //means item belong to a locked bucket remove item from all unlocked buckets
                __item.removeFromBuckets();
            }
            return false;
        },

        /**
         * Fully dereferences cache item from its pool
         * Removing listeners and adjusting the pool memory, length accordingly
         * @param   {Number|String} __hash  String or Number
         * @param   {Object} __item CacheItem instance
         * @param   {Number} __size    Item memory
         * @returns {Object} this instance
         */
        __derefferenceItem__: function(__hash, __item, __size) {
            //else - delete reference and resolve item attributes
            delete this.__cache__[__hash];
            this.length -= 1;
            //resolve listener reference 0 which is this exact method, but with binding calls - it creates new method
            //and therefor can never be identified by the .removeListener method - not-a-hack!
            __item.removeListener('destroyed', this.__copyOnItemDestroyed__);
            __item.removeListener('sizeUpdate', this.__copyOnItemUpdate__);
            __item.removeListener('error', this.__copyOnError__);
            //decrease size
            this.__memorySize__ -= __size;
            //normalize
            /*if (this.__memorySize__ < 1e-6) {
                this.__memorySize__ = 0;
            }*/
            this.verbose('hash "' + __hash + '" - removed!', 'size: ' + __size, 'length:' + this.length);
            return this;
        },



        //-----------------------------------------     GET     ---------------------------------------------



        /**
         * Returns name of pool|bucket
         * @returns {String} name of pool|bucket
         */
        getName: function() {
            return this.name;
        },

        /**
         * Returns count of cache items
         * @returns {Number} count of cache items
         */
        getLength: function() {
            return this.length;
        },

        /**
         * Finds all locked items associated with pool and pushes it all into array
         * @returns {Array} array of locked items or empty array
         */
        getLocked: function() {
            var _array;
            //set time last used
            this.__getLockedTime = Date.now();
            _array = this.getArray().filter(lockFilter);
            return _array;
        },

        /**
         * Finds all locked items associated with pool and pushes it all into array
         * @returns {Array} array of locked items or empty array
         */
        getNotLocked: function() {
            var _array;
            //set time last used
            this.__getLockedTime = Date.now();
            _array = this.getArray().filter(notLockedFilter);
            return _array;
        },

        /**
         * return all cached pool items as array
         * @returns {Array} collection of cached items
         */
        getArray: function() {
            var _collection = [];

            this.forEach(function(__poolItem) {
                _collection.push(__poolItem);
            });

            return _collection;
        },

        /**
         * Returns current weight in KB
         * @returns {Number} KB memory usage
         */
        getMemorySize: function() {
            return this.__memorySize__;
        },

        /**
         * Stats of a last update() duration
         * @returns {Number} -1 - no update ever done, else a time in MS
         */
        getLastUpdateStats: function() {
            return this.__updateDuration__;
        },



        //-----------------------------------------     CLEAR     ---------------------------------------------


        /**
         * Bulk scan to free time based configuration
         * @param   {Number} __lifespan time duration of age targeting
         * @param   {Boolean} __force  ignore lock and linkage
         * @returns {Object} this instance
         */
        croneFree: function(__lifespan, __force) {
            if (!$assertful.optBoolean(__force) || !$assertful.optPint(__lifespan)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR), 'Use boolean and number arguments only!');
                return this;
            }
            return this.__croneFree__(__lifespan, __force);
        },

        /**
         * No argument check performed when using protected method
         * Bulk scan to free time based configuration
         * @param   {Number} __lifespan time duration of age targeting
         * @param   {Boolean} __force ignore lock and linkage
         * @returns {Object} this instance
         * @protected
         */
        __croneFree__: function(__lifespan, __force) {
            this.__croneFreeTime = Date.now();
            this.forEach(function(__item) {
                //use if statement as function call then compare result - then use next available crone clean
                if (this.__removeExpiredItem__(__force, __item) !== true) {
                    this.__removeOlderThanItem__(__force, __lifespan, __item);
                }
            }.bind(this));
            return this;
        },


        /**
         * Removes all expired items based on lifespan of item
         * @param {Boolean} __force Optional flag to force destruction of item if locked
         * @returns {Object} this instance
         */
        clearExpired: function(__force) {
            if (!$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean as argument only!', arguments));
                return this;
            }
            return this.__clearExpired__(__force);
        },

        /**
         * Warning! No argument validity is checked - use at your own risk!
         * Removes all expired items bases on lifespan of item
         * @param {Boolean} __force Optional flag to force destruction of item if locked
         * @returns {Object} this instance
         * @protected
         */
        __clearExpired__: function(__force) {
            this.__clearExpiredTime = Date.now();
            this.forEach(this.__removeExpiredItem__.bind(this, __force));
            return this;
        },

        /**
         * Warning! - no argument checks are performed - use at your own risk!
         * Removes pool items if exceeded provided lifetime
         * @param {Boolean} __force Optional, null, force removal if item locked
         * @param {Object} __poolItem instance of PoolItem
         * @returns {Boolean} true if item was removed
         */
        __removeExpiredItem__: function(__force, __poolItem) {
            if (__poolItem.isExpired()) {
                this.verbose('Item Expired');
                return this.__removeItem__(__poolItem, __force);
            }
            return false;
        },

        /**
         * Removes items that are older than a given lifespan or a a value defined in configuration
         * @param {Number} __lifespan Optional restrict lifetime override - else Config will be used
         * @param {Boolean} __force Optional flag to force destruction of item if locked
         * @returns {Object} this instance
         */
        clearOld: function(__lifespan, __force) {
            var _lifespan;
            if (!$assertful.optPint(__lifespan) || !$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean and number arguments only!', arguments));
                return this;
            }
            _lifespan = __lifespan || this.config.getLifespan();
            //at this point _lifespan can be int or null|undefined
            return this.__clearOld__(_lifespan, __force);
        },

        /**
         * Warning! No argument validity is checked - use at your own risk!
         * Removes all expired items bases on lifespan of item
         * @param {Number} __lifespan restrict lifetime - else no action will be taken
         * @param {Boolean} __force Optional flag to force destruction of item if locked
         * @returns {Object} this instance
         * @protected
         */
        __clearOld__: function(__lifespan, __force) {
            if (!__lifespan) {
                return this;
            }
            this.__clearOldTime = Date.now();
            //if lifespan was specified - perform comparison of items lifetimes
            //at this point if the item itself was not expired, but outlives the specified lifespan - it will be removed
            this.forEach(this.__removeOlderThanItem__.bind(this, __force, __lifespan));
            return this;
        },

        /**
         * Warning! - no argument checks are performed - use at your own risk!
         * Removes pool items if exceeded provided lifetime
         * @param {Boolean} __force Optional, null, force removal if item locked
         * @param {Number} __lifespan Optional, null, time duration in ms
         * @param {Object} __poolItem instance of PoolItem
         * @returns {Boolean} true if item was removed
         */
        __removeOlderThanItem__: function(__force, __lifespan, __poolItem) {
            if (__poolItem.isOlderThan(__lifespan)) {
                this.verbose('Item is too old');
                return this.__removeItem__(__poolItem, __force);
            }
            return false;
        },

        /**
         * Deleted oldest items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked anyways
         * @returns {Object}  this instance
         */
        clearFIFO: function(__amount, __force) {
            //if no amount is provided then nothing to clean really
            //strict check for non optional argument
            if (!$assertful.pint(__amount) || !$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean and number arguments only!', arguments));
                return this;
            }
            //call optimized method
            return this.__clearFIFO__(__amount, __force);
        },

        /**
         * Private method -
         * WARNING - no checks are performed - use at your own risk!
         * Deleted oldest items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked aways
         * @returns {Object}  this instance
         */
        __clearFIFO__: function(__amount, __force) {
            var _collection;
            //set time last used
            this.__clearFIFOTime = Date.now();
            //get array of items and perform sort to have oldest items come first
            _collection = this.getArray().sort(oldestItemSort);
            //remove just the amount needed to be cleaned
            this.__removeAmount__(__amount, __force, _collection);
            return this;
        },

        /**
         * Deletes least frequently used items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked aways
         * @returns {Object} this instance
         */
        clearLFU: function(__amount, __force) {
            //if no amount is provided then no thing to clean really
            if (!$assertful.pint(__amount) || !$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean and number arguments only!', arguments));
                return this;
            }
            //call optimized method
            return this.__clearLFU__(__amount, __force);
        },

        /**
         * Warning! No argument validity check are performed, use at your own risk!
         * Deleted least frequently used items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked aways
         * @returns {Object} this instance
         */
        __clearLFU__: function(__amount, __force) {
            var _collection;
            //set time last used
            this.__clearLFUTime = Date.now();
            //get array of items and perform sort to have least items used come first
            _collection = this.getArray().sort(LFUSort);
            //remove just the amount needed to be cleaned
            this.__removeAmount__(__amount, __force, _collection);
            return this;
        },

        /**
         * Deleted least frequently used items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked aways
         * @returns {Object} this instance
         */
        clearLRU: function(__amount, __force) {
            //if no amount is provided then no thing to clean really
            if (!$assertful.pint(__amount) || !$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean and number arguments only!', arguments));
                return this;
            }
            //call optimized method
            return this.__clearLRU__(__amount, __force);
        },

        /**
         * Warning! No argument validity check are performed, use at your own risk!
         * Deleted least frequently used items from collection - but only the amount specified
         * @param   {Number} __amount Number of items needed to be destroyed
         * @param   {Boolean} __force Optional destroy item if locked anyways
         * @returns {Object} this instance
         */
        __clearLRU__: function(__amount, __force) {
            var _collection;
            //set time last used
            this.__clearLRUTime = Date.now();
            //get array of items and perform sort to have least items used come first
            _collection = this.getArray().sort(LRUSort);
            //remove just the amount needed to be cleaned
            this.__removeAmount__(__amount, __force, _collection);
            return this;
        },

        /**
         * Clears all items
         * @param   {Boolean} __force if true - will remove item from all pools it associated with
         * @returns {Number} length of pool
         */
        clearAll: function(__force) {
            //perform check
            if (!$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Use boolean as argument only!', arguments));
                return this;
            }
            //call optimized method
            return this.__clearAll__(__force);
        },

        /**
         * Clears all items
         * @param   {Boolean} __force if true - will remove item from all pools it associated with
         * @returns {Object} this instance
         */
        __clearAll__: function(__force) {
            //set time last used
            this.__clearAllTime = Date.now();
            //get array of items and perform sort to have least items used come first
            this.forEach(function(__item) {
                // call optimized method
                this.__removeItem__(__item, __force);
            }.bind(this));

            //destroy all pool items
            if (__force && (this.length > 0 || !$math.near(this.__memorySize__, 0))) {
                this.emit('error', $error($error.TYPE.MEMORY_LEAK, 'Memory leak detected!'));
            }

            return this;
        },

        /**
         * Expires all associated items
         * @returns {Object} this instance
         */
        expireAll: function() {
            //set time last used
            this.__expireAllTime = Date.now();
            //get array of items and perform sort to have least items used come first
            this.forEach(function(__item) {
                // call optimized method
                __item.setLifespan(0);
            });

            return this;
        },



        //-----------------------------------------     TEST     ---------------------------------------------


        /**
         * Identifies item of own type and called override method
         * to test for item usability
         * @param   {Object} __item Cache Item instance
         * @returns {Boolean} true - not used
         */
        canDestroyItem: function(__item) {
            return this.isOwnItem(__item) && this.isItemUsed(__item);
        },

        /**
         * WARNING - optimized version of public method
         * Identifies item of own type and called override method
         * to test for item usability
         * @param   {Object} __item Cache Item instance
         * @returns {Boolean} true - not used
         */
        __canDestroyItem__: function(__item) {
            return this.__isOwnItem__(__item) && this.isItemUsed(__item);
        },

        //override this method to track your items
        isItemUsed: function() {
            return false;
        },

        /**
         * Check is item belongs to this pool
         * @param   {Object}  __item Cache Item sub-classed instance
         * @returns {Boolean} true if of this pool
         */
        isOwnItem: function(__item) {
            if (!$Item.isOffspring(__item) || !__item.instanceof($Item)) {
                return false;
            }

            if (__item.getType() !== this.config.getType()) {
                return false;
            }

            return this.__isOwnItem__(__item);
        },

        /**
         * Check is item belongs to this pool
         * @param   {Object}  __item Cache Item sub-classed instance
         * @returns {Boolean} true if of this pool
         */
        __isOwnItem__: function(__item) {
            return this.__findItemByHash__(__item.getHash(), true) === __item;
        },

        /**
         * checks own size against the capacity of Config
         * @returns {Boolean} true of overflown
         */
        __isOverflown: function() {
            return this.__memorySize__ > 0 && this.config.getCapacity() < this.__memorySize__;
        },

        /**
         * Boolean check is tests pool type is of a master pool
         * Master pools configured first using PoolConfig.init()
         * @returns {Boolean} true is master pool
         */
        isMasterPool: function() {
            return this.config.getMemoryType().pool === this;
        },



        //-----------------------------------------     FIND     --------------------------------------------



        /**
         * Retrieves cache pool item of key
         * @param   {Hash} __hash Item key
         * @returns {Mixed}  cached value - cargo i=of pool item
         */
        findItemByHash: function(__hash) {
            //must test fo valid key else it'll blow up
            //we don't want to blow up loud
            if (!$assertful.hash(__hash)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Invalid key was used to get cache', __hash));
                return null;
            }
            return this.__findItemByHash__(__hash);
        },

        /**
         * Warning! No validation is performed, use at your own risk!
         * Retrieves cache pool item of key
         * @param   {Hash} __hash Item key
         * @param {Boolean} __silentSearch if true - does call item.update()
         * @returns {Mixed}  cached value - cargo i=of pool item
         */
        __findItemByHash__: function(__hash, __silentSearch) {
            var _poolItem;
            //search for cache by hash
            _poolItem = this.__cache__[__hash];

            if (!_poolItem) {
                return null;
            }
            if (__silentSearch !== true) {
                //must update the item access time to keep the LFU logical
                _poolItem.update();
            }

            return _poolItem;
        },



        //-----------------------------------------     MANAGE     --------------------------------------------



        /**
         * Private method - DO NOT USE
         * Check if capacity is overflown and perform clean based on Config instructions
         * @param   {Boolean} __force this will clear locked items
         * @param {Number} __maxLifetime duration of a lifetime beyond which items will be removed
         * @returns {Object} this instance
         */
        update: function(__force, __maxLifetime) {
            //chec optional arguments
            if (!$assertful.optTrue(__force) || !$assertful.optPint(__maxLifetime)) {
                this.emit($error($error.TYPE.TYPE_ERROR, 'invalid arguments', arguments));
                return this;
            }
            //use optimized call
            return this.__update__(__force, __maxLifetime);
        },

        /**
         * Warning! No validation of arguments is performed - use at your own risk!
         * Logs time and launches scan
         * @param   {Boolean} __force ignore locked flags of items
         * @param   {Number} __maxLifetime lifetime in ms - older than this items will be targeted
         * @returns {Object} this instance
         */
        __update__: function(__force, __maxLifetime) {
            //register time before clearing
            var _time = Date.now();

            if (this.__isOverflown()) {
                //cache pool overflown its capacity by size - emit event to be custom handled
                this.emit('overflown', __force, __maxLifetime);
            }

            if (this.__isOverflown() || (this.__scanTime && Date.now() - this.__scanTime > this.config.getMaintInteval())) {
                //if overflown or
                //if never scanned before or
                //if not overflown and time exceeded MAINTENANCE_INTERVAL
                // - perform a maintenance scan
                this.__collect__(__force, __maxLifetime);
            }

            _time = Date.now() - _time;
            this.__updateDuration__ = _time;
            this.verbose('Update Time duration: ' + _time + ' ms');
            return this;
        },

        /**
         * Private method - DO NOT USE as no checks are performed!
         * Main concept is to have this pool survived the overflow, which means - every possible action will be taken in order to
         * sustain the capacity below the MAX!
         * Check is capacity is overflown and perform clean based on Config instructions
         * @param   {Boolean} __force this will clear locked items
         * @param {Number} __maxLifetime expiry lifespan
         * @returns {Boolean} true if updated
         */
        __collect__: function(__force, __maxLifetime) {
            var _aproxPerItemSize, _overflownSize, _amountToClear, _detectedAmountNeeded;
            var _startLength = this.length;
            this.__scanTime = Date.now();

            //always perform expiry cleanup
            //expiry time may be defined by Config, else items expiry time will be used
            //crone iterates over all items in the pool
            this.__croneFree__(__maxLifetime, __force);
            //check the status only after expired and olf items cleaned up
            //remember - since expiry is optional in both item and pool Config - this may not result in any item removal
            if (_startLength !== this.length) {
                this.verbose('Memory was freed by Expiry, items removed:' + (_startLength - this.length));
            }

            if (!this.__isOverflown()) {
                return this;
            }

            //proceed to figure out amount needed
            _aproxPerItemSize = this.__memorySize__ / this.length;
            //find out how much memory needed to be freed to return to the threshold value
            _overflownSize = this.__memorySize__ - this.config.getCapacity() * THRESHHOLD;
            //find out how many items needed to be removed - optimized way to do a bulk scan
            _detectedAmountNeeded = Math.ceil(_overflownSize / _aproxPerItemSize);
            //normalize against .length
            _amountToClear = Math.min(_detectedAmountNeeded, this.length);

            if (!$assertful.positive(_amountToClear) || _amountToClear === this.length) {
                throw 'Something went wrong! Items report invalid size or pool/bucket is miss-configured';
            }

            this.verbose('Proceeding to free: ~' + _overflownSize + ' KB',
                'items: ~' + _amountToClear,
                'current: ' + this.__memorySize__ + ' KB',
                'items: ~' + this.length,
                'expected: ~' + (this.__memorySize__ - _overflownSize) + 'KB');

            //Clear according configuration rules
            if (this.config.isFIFO()) {
                this.__clearFIFO__(_amountToClear, __force);
            } else if (this.config.isLFU()) {
                this.__clearLFU__(_amountToClear, __force);
            } else if (this.config.isLRU()) {
                this.__clearLRU__(_amountToClear, __force);
            }

            if (!this.__isOverflown()) {
                this.verbose('Memory was freed by FIFO|LFU|LRU: ~' + _overflownSize + ' KB',
                    'current: ' + this.__memorySize__ + ' KB',
                    'items: ~' + this.length);
                return this;
            }

            //still overflowing and unable to even force it through?
            if (__force) {
                this.emit('error', $error($error.TYPE.UNABLE_TO_FREE_WITH_FORCE, 'Unable to free memory'));
                return this;
            }
            //try forcing it - this will clear locked items
            this.emit('error', $error($error.TYPE.UNABLE_TO_FREE, 'All items seem to be used or locked - Collecting locked|Used Items', arguments));
            return this;
        },



        //-----------------------------------------     EVENTS     --------------------------------------------



        /**
         * Each cache item has attached listener for "sizeUpdate" event that should be emitted -
         * for every time data is changed on the cache item
         * this will lead to the update to be triggered
         * This method adjusts memory according to saved and current item size
         * @param   {Object} __item item that emitted event
         */
        __onItemUpdate__: function(__item) {
            var _newSize = __item.getMemorySize(this.config.getType());
            var _origSize = __item.getSavedSize();
            this.__memorySize__ -= _origSize;
            this.__memorySize__ += _newSize;
            //call optimized method only if change is bigger
            if (_newSize > _origSize) {
                this.__update__();
            }
        },

        /**
         * Event emitted by cache item when destroyed
         * Remove key (dereference to be picked up by JSGC) and reduce current weight
         * @param   {Object} __item Item instance
         * @returns {Object} this instance
         */
        __onItemDestroyed__: function(__item) {
            //force removal if destroyed!
            return this.__removeItem__(__item, true, true);
        },

        /**
         * Private on error handler
         * If able to handle type error - resolves the error,
         * else throws it up the stack
         * @param   {Object} __instance caller
         * @param   {Object} __err instance of Error
         */
        __onError__: function(__instance, __err) {
            //for now - destroy Item
            switch (__err.type) {
                case $error.TYPE.CORRUPTED_ITEM:
                    __instance.__destroy__();
                    break;
                case $error.TYPE.UNABLE_TO_FREE:
                    //a default attempt at forcing error
                    __instance.__collect__(true, __err.args[0], __err.args[1]);
                    break;
                default:
                    throw __err;
            }
        },



        //-----------------------------------------     NODE     --------------------------------------------



        /**
         * Adds pool node link to Pool
         * @param {module:cacheable/bucket~Bucket} __node Pool node
         * @returns {Object} this instance
         */
        addBucket: function(__node) {
            if (this.__linkedBuckets__.indexOf(__node) < 0) {
                this.__linkedBuckets__.push(__node);
                this.verbose('Linked Bucket to Pool: ' + __node.getName());
            }
            return this;
        },

        /**
         * Removed node object from a linked array
         * @param   {module:cacheable/bucket~Bucket} __node [description]
         * @returns {Object} this instance
         */
        removeBucket: function(__node) {
            var _index = this.__linkedBuckets__.indexOf(__node);
            if (_index >= 0) {
                this.__linkedBuckets__.splice(_index, 1);
                this.verbose('Unlinked Bucket from Pool: ' + __node.getName());
            }
            return this;
        },

        /**
         * Removed node object from a linked array
         * @param   {module:cacheable/bucket~Bucket} __node [description]
         * @returns {Object} this instance
         */
        getBuckets: function() {
            return this.__linkedBuckets__.slice(0);
        },

        /**
         * Finds all linked to buckets items
         * @returns {Array} array of linked items
         */
        getLinkedItems: function() {
            return this.getArray().filter(linkedFilter);
        },

        /**
         * Finds all NOT linked to buckets items
         * @returns {Array} array of NOT linked items
         */
        getNotLinkedItems: function() {
            return this.getArray().filter(notLinkedFilter);
        },

        /**
         * Finds total size (memory) of linked to buckets items
         * @returns {Number} total linked size
         */
        getLinkedSize: function() {
            var _collection = this.getArray().filter(linkedFilter);
            var _size = 0;
            _collection.forEach(function(__item) {
                _size += __item.getMemorySize(this.config.getType());
            }.bind(this));

            return _size;
        },
        /**
         * Finds total size (memory) of NOT linked to buckets items
         * @returns {Number} total NOT linked size
         */
        getNotLinkedSize: function() {
            var _collection = this.getArray().filter(notLinkedFilter);
            var _size = 0;
            _collection.forEach(function(__item) {
                _size += __item.getMemorySize(this.config.getType());
            }.bind(this));

            return _size;
        },



        //-----------------------------------------     BULC ACTIONS     --------------------------------------------



        /**
         * Loops over cache executing provided method on each
         * If return of execution is NOT undefined - breaks the loop and return that value
         * @param   {Function} __func Method that will be executed for each pool item in cache
         * @returns {Object} this instance
         */
        forEach: function(__func) {
            var _i, _key, _keys, _poolItem;

            if (!$assertful.func(__func)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'not a function!'));
                return this;
            }
            //during iteration items may be removed
            //so its always best to iterate over saved keys array
            _keys = Object.keys(this.__cache__);

            for (_i = 0; _i < _keys.length; _i += 1) {
                _key = _keys[_i];
                _poolItem = this.__cache__[_key];
                __func(_poolItem);
            }

            return this;
        },

        /**
         * Locks all cached items of pool regardless of current item state
         * @param {Array} __exceptionArray Optional, if provided filters array values out from the return
         * @returns {Object} this instance
         */
        lockAll: function(__exceptionArray) {
            if (!$assertful.optNearray(__exceptionArray)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, '.lockAll() invalid arguments!', arguments));
                return this;
            }
            return this.__lockAll__(__exceptionArray);
        },

        /**
         * Locks all cached items of pool regardless of current item state
         * @param {Array} __exceptionArray Optional, if provided filters array values out from the return
         */
        __lockAll__: function(__exceptionArray) {
            //set time last used
            this.__lockAllTime = Date.now();

            if (__exceptionArray) {
                this.forEach(function(__item) {
                    if (__exceptionArray.indexOf(__item) < 0) {
                        __item.lock();
                    }
                });
            } else {
                this.forEach(function(__item) {
                    __item.lock();
                });
            }
        },

        /**
         * Locks all cached items of pool regardless of current item state
         * @param {Array} __exceptionArray Optional, if provided filters array values out from the return
         * @returns {Object} this instance
         */
        unlockAll: function(__exceptionArray) {
            if (!$assertful.optNearray(__exceptionArray)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, '.unlockAll() invalid arguments!', arguments));
                return this;
            }
            return this.__unlockAll__(__exceptionArray);
        },

        /**
         * Locks all cached items of pool regardless of current item state
         * @param {Array} __exceptionArray Optional, if provided filters array values out from the return
         */
        __unlockAll__: function(__exceptionArray) {
            //set time last used
            this.__lockAllTime = Date.now();

            if (__exceptionArray) {
                this.forEach(function(__item) {
                    if (__exceptionArray.indexOf(__item) < 0) {
                        __item.unlock();
                    }
                });
            } else {
                this.forEach(function(__item) {
                    __item.unlock();
                });
            }
        },



        //-----------------------------------------     STATS     --------------------------------------------



        /**
         * Check current size against reported by items
         * Throws error if miss-match detected!
         */
        logStats: function() {
            var _actualMem = 0;
            var _type = this.config.getType();
            var _units = this.config.getUnits();

            //collect data
            this.forEach(function(__item) {
                _actualMem += __item.getMemorySize(_type);
            });

            $logger.log('Stats:', '\t\ttype:' + _type.name + ', size:[' + this.getMemorySize() + '' + _units + '], length:[' + this.length + '], overflown:[' + (this.getMemorySize() - this.config.getCapacity()) + '' + _units + ']');
        }
    });
    return Pool;
});
