/**
 * Common error types definitions
 */
define(['p!-error'], function($error) {
    'use strict';

    var type = {
        UNABLE_TO_FREE: 'UNABLE_TO_FREE',
        UNABLE_TO_FREE_WITH_FORCE: 'UNABLE_TO_FREE_WITH_FORCE',
        CORRUPTED_ITEM: 'CORRUPTED_ITEM',
        MEMORY_LEAK: 'MEMORY_LEAK',
        ITEM_NOT_FOUND: 'ITEM_NOT_FOUND',
        ITEM_MISSMATCH: 'ITEM_MISSMATCH',
        CONFIG: 'CONFIG'
    };

    var ErrorClass = $error.subclass('CacheableError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
