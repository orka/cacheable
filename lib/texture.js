define(['p!-texture',
    'p!-assertful',
    'p!-logger',
    'p!-cacheable/item',
    'p!-cacheable/image',
    'p!-cacheable/config',
    'p!-texture/error'
], function($texture,
    $assertful,
    $logger,
    $CacheItem,
    $cacheImage,
    $cacheConfig,
    $error) {
    'use strict';


    var CacheTexture;

   /*
   var SYSTEM_MASTER_SET = false;
   function checkSystemCache() {
        if (SYSTEM_MASTER_SET) {
            return;
        }
        if ($cacheConfig.TYPE.SYSTEM.pool) {
            if ($cacheConfig.TYPE.SYSTEM.pool.config.getCapacity()) {

            }
        }
    }
*/
    //DOTO
    //Ensure that data size events are deliniated to ensure diferent cache mechanixm responds accordingly
    //best to represent this behaviour in the tecture definition in cacheable
    CacheTexture = $CacheItem.subclass('CacheTexture', {
        constructor: function(__url, __options, __throttler) {
            $texture.texture.prototype.constructor.call(this, __url, __options, __throttler);
            CacheTexture.super.constructor.call(this, $cacheConfig.TYPE.VIDEO, this.url, __options);
            this.addListener('loading', this.__whileTextureImageLoading__.bind(this));
            this.addListener('loaded', this.__whenTextureImageLoaded__.bind(this));
        },
        deconstructor: function() {
            CacheTexture.super.deconstructor.call(this);
            //evoke protoype deconstructor of Image
            $texture.texture.prototype.deconstructor.call(this);
        }
    });

    //!IMPORTANT - MIXIN texture APIs!
    CacheTexture.mixin($texture.texture.prototype);
    //override texture APIs
    CacheTexture.mixin({
        /**
         * Locks image while texture is being made
         */
        __whileTextureImageLoading__: function() {
            if (!this.__image.isLocked()) {
                this.__image.lock();
                this.__customLock = true;
            }
        },

        /**
         * Releases lock in image if was originally set to retain image for texture creation
         */
        __whenTextureImageLoaded__: function() {
            if (this.__customLock && this.__image) {
                this.__image.unlock();
            }
        },
        /**
         * Creates Image Class instance with arguments obtained at this instance construction
         * @param   {Boolean} __force if true, ignores currently set image and creates a new image instance
         * @returns {Boolean}         true if new image was created
         */
        __createImage__: function(__force) {
            var _image, _url;
            if (!$assertful.optBoolean(__force)) {
                this.emit('error', $error($error.TYPE.INVALID_ARGUMENT, 'Only Boolan type allowed'));
                return false;
            }

            if (this.__image && !__force) {
                return false;
            }

            //retrieve URL as an assigned value due to transform URL ability of image.
            //If object had image previously then local url data may have been transformed and cache will be missmatched
            _url = this.url || this.__initArguments[0];
            //fetch image from SYSTEM CACHE (!)
            _image = $cacheConfig.TYPE.SYSTEM.pool.findItemByHash(_url);
            //
            if (!_image) {
                //else create a new CACHE image and throw it directly into SYSTEM MASTER CONFIG!
                _image = $cacheImage(_url, this.__initArguments[1], this.__initArguments[2]);
                //use optimized method of cache pool
                $cacheConfig.TYPE.SYSTEM.pool.__addItem__(_image);
                this.__image = _image;
            } else {
                this.__image = _image;
            }

            //set listeners always
            //if image already exists - all memory related attributes will be set immediately
            this.__assignImageListeners();
            return true;
        },

        /**
         * evrride get data method
         * @returns {Number} a file size of image / compressed
         */
        getMemorySize: function() {
            //if image not loaded but has size defined in its constructor arguments
            //the preallocated size is returned
            return this.getGPUSize();
        },

        getScaledSize: function () {
            return this.__image.scaledSize;
        }
    });

    CacheTexture.DEFAULT_TEXTURE = $texture.texture.DEFAULT_TEXTURE;
    CacheTexture.DEFAULT_URL = $texture.texture.DEFAULT_URL;
    CacheTexture.DEFAULT_CONFIG = {texture: $texture.texture.DEFAULT_CONFIG, cacheItem: $CacheItem.DEFAULT_CONFIG};
    return CacheTexture;
});
