define([
    'p!-cacheable/item',
    'p!-cacheable/config'
], function (
    $cacheableItem,
    $cacheableConfig
) {
    'use strict';

    var CacheMetadata = $cacheableItem.subclass('CacheMetadata', {
        constructor: function(__options) {
            Metadata.prototype.constructor.call(this, __options);
            CacheMetadata.super.constructor.call(this, $cacheableConfig.TYPE.SYSTEM, __options.hash, __options);
        },
        deconstructor: function() {
            CacheMetadata.super.deconstructor.call(this);
            //Metadata.prototype.deconstructor.call(this); // Metadata doesn't implement a destructor
        }
    });

    CacheMetadata.prototype.getMemorySize = function() {
        return JSON.stringify(this.data).length / 1024; // approximate size of JSON object in KB
    };

    CacheMetadata.prototype.clearData = function() {
        delete this.data;
    };

    CacheMetadata.getHash = function (__url, __params, __serverArgs) {
        return [__url, JSON.stringify(__params), JSON.stringify(__serverArgs)].join(', ');
    };

    function Metadata(__options) {
        this.data = __options.data;
    }

    return CacheMetadata;
});
