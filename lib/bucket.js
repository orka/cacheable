define(['p!-cacheable/pool', 'p!-assertful', 'p!-defineclass', 'p!-cacheable/error',  'p!-logger'], function($Pool, $assertful, $class, $error, $logger) {
    'use strict';

    /**
     * @class cacheable/bucket
     * @name CacheBucket
     * @classdesc A bucket class is a subclass of cacheable/pool.
     * CacheBucket is a "node" of a master pool, it does however keeps items reference of its own
     * and has a special hand=ling of Add and Remove methods
     */
    var CacheBucket = {
        /**
         * @constructor
         * @param   {CacheBucketConfig} __config Instance
         * @param   {String} __name Name of Bucked instance
         */
        constructor: function(__config, __name) {
            var _master;
            CacheBucket.super.constructor.call(this, __config, __name);

            _master = __config.getType().pool;
            //if master was never configured or not an offspring of Pool or not a master
            if (!$Pool.isOffspring(_master) || !_master.isMasterPool()) {
                throw $error($error.TYPE.CONFIG, 'Master Cache must be initialized before creating nodes', null, arguments);
            }

            if (_master.config.getCapacity() < __config.getCapacity()) {
                throw $error($error.TYPE.CONFIG, 'Capacity of bucket configurations cannot exceed that of master!', __config);
            }

            this.__locked__ = false;
            this.__timeCreated = Date.now();
            this.__masterPool__ = _master;
            //ensure to listen to when the master is destroyed
            this.__masterPool__.addListener('destroyed', this.deconstructor.bind(this));
            this.__masterPool__.addBucket(this);
        },

        /**
         * Dispose the bucket.
         * @exports cacheable/bucket
         */
        deconstructor: function() {
            CacheBucket.super.deconstructor.call(this);
            this.__masterPool__.removeBucket(this);
            this.__masterPool__ = null;
            this.__locked__ = null;
            this.__timeCreated = null;
        },

        /**
         * Returns the master pool of Bucket
         * @returns {Object} Pool instance (Master only)
         */
        getMaster: function() {
            return this.__masterPool__;
        },

        /**
         * Overrides pool method to ensure the item is linked to the bucket if found
         * @param   {String|Number} __hash Item hash
         * @returns {Object|Null}  null | Object item
         */
        findItemByHash: function(__hash) {
            var _item = CacheBucket.super.findItemByHash.call(this, __hash);
            //Item found in Bucket - return
            if (_item) {
                return _item;
            }
            //lock for it in Master Pool private optimized method
            _item = this.__masterPool__.__findItemByHash__(__hash);
            //add item into Bucket using super method
            if (_item) {
                CacheBucket.super.__addItem__.call(this, _item);
                _item.addBucket(this);
            }
            return _item;
        },


        /**
         * adds self as a stacking node to the item, then add item to master and self
         * @param {*} __cacheItem    FOO
         * @param {*} __override     FOO
         * @param {*} __bypassUpdate FOO
         * @returns {*} FOO
         */
        __addItem__: function(__cacheItem, __override, __bypassUpdate) {
            //add both to master and own cache
            //ORDER MATTERS - only after bucket was referenced - reference Master
            CacheBucket.super.__addItem__.call(this, __cacheItem, __override, __bypassUpdate);
            //if master is overflown it will trigger clean up in both - buckets and itself
            this.__masterPool__.__addItem__(__cacheItem, __override, __bypassUpdate);
            __cacheItem.addBucket(this);
            return this;
        },

        /**
         * Add all values of given array to the pool cache using .addItem method
         * @param {Array} __array collection of cacheables
         * @param {Boolean} __override Optional if true same key pool item will be destroyed and replaced be a new item
         * @param {Boolean} __bypassUpdate if true does not trigger pool scan action
         * @returns {Object} this instance
         */
        addArray: function(__array, __override, __bypassUpdate) {
            //call original method
            CacheBucket.super.addArray.call(this, __array, __override, __bypassUpdate);
            //the explicitly update master
            this.__masterPool__.__update__();
            return this;
        },

        /**
         * removes self as a tracking node from the item, then calls super - same method
         * @param   {*} __cacheItem        FOO
         * @param   {*} __force            FOO
         * @param   {*} __alreadyDestroyed FOO
         * @returns {*}                    FOO
         */
        __removeItem__: function(__cacheItem, __force) {
            var _size, _hash;
            if (__force !== true && this.isLocked()) {
                this.verbose('Unable to remove item! Bucket is locked');
                return this;
            }
            _size = __cacheItem.getMemorySize(this.config.getMemoryType());
            _hash = __cacheItem.getHash();

            __cacheItem.removeBucket(this);
            // if NOT a master pool, only dereference item!
            // this is done by "false" flag
            this.__derefferenceItem__(_hash, __cacheItem, _size);

            return this;
        },

        /**
         * Sets bucket as locked which will have an effect of a locked item
         * for all associated items
         * @param {Boolean} __true value to which to set the lock state
         * @returns {module:cacheable/bucket~CacheBucket} this instance
         */
        setLock: function(__true) {
            if (__true === true) {
                this.lock();
            } else if (__true === false) {
                this.unlock();
            }
            return this;
        },

        /**
         * Sets current lock state to "locked"
         * @returns {module:cacheable/bucket~CacheBucket} this instance
         */
        lock: function() {
            this.__locked__ = true;
            return this;
        },

        /**
         * Sets current lock state to "unlocked"
         * @returns {module:cacheable/bucket~CacheBucket} this instance
         */
        unlock: function() {
            this.__locked__ = false;
            return this;
        },

        /**
         * Simple check of a locked state
         * @returns {Boolean} true if locked
         */
        isLocked: function() {
            return this.__locked__ === true;
        }
    };

    CacheBucket = $Pool.subclass('CacheBucket', CacheBucket);
    return CacheBucket;
});
