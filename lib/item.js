/**
 * Anything cacheable is a cacheItem.
 * Use this class to extent your objects Image, Meta, Texture etc...
 */
define(['p!-eventful',
    'p!-assertful',
    'p!-cacheable/error',
    'p!-defineclass',
    'p!-logger'
], function($eventful,
    $assertful,
    $error,
    $classify,
    $logger) {
    'use strict';
    var DEFAULT_CONFIG = {
        lifespan: null,
        locked: false
    };
    /**
     * Class API
     * @type {Object}
     */
    var ItemClass = {

        /**
         * Sets up variables and assign arguments provided
         * Throws error if associated key function is not provided
         * @param {Object} __type Type Cache
         * @param {*} __hash Number or String - HASH
         * @param  {Object} __object  Optional - {lifespan: Number "expiry configuration", locked: Boolean "lock in master cache"}
         */
        constructor: function(__type, __hash, __object) {
            var _nObject;
            //initialize super
            ItemClass.super.constructor.call(this);
            //check arguments
            if (!$assertful.object(__type) || !$assertful.hash(__hash) || !$assertful.optObject(__object)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Invalid arguments', arguments);
            }
            //fall-back to default if options not defined
            _nObject = __object || DEFAULT_CONFIG;
            if (!$assertful.optPint(_nObject.lifespan)) {
                throw $error($error.TYPE.TYPE_ERROR, 'lifespan option must be a positive integer', arguments);
            }
            if (!$assertful.optBoolean(_nObject.locked)) {
                throw $error($error.TYPE.TYPE_ERROR, 'locked options must be a boolean', arguments);
            }
            this.__type__ = __type;
            this.__hash = __hash;
            this.__lifespan = _nObject.lifespan;
            this.__timeCreated = Date.now();
            this.__timesUsed = -1; // set to negative value as .update function increments it by 1
            //lock immediately
            //lock is an independent from nodes used linkage idea
            //item can be locked, bot not used by any nodes and vice-versa
            //depending on the type of pool master/node the logic of destruction and other APIs is different in regards to lock
            this.setLock(!!_nObject.locked);
            this.__buckets__ = [];
            // this.__originalSize = this.getMemorySize();
            this.update();

            this.addListener('dataSize', this.__onSizeChange__.bind(this));
            this.addListener('used', this.update.bind(this));
        },

        /**
         * Destroys all set variables
         */
        deconstructor: function() {
            this.emit('deconstructed');
            //this should take care of data associated with object
            this.clearData();

            this.__timeCreated = null;
            this.__lastUsedTime = null;
            this.__hash = null;
            this.__locked__ = null;
            this.__lifespan = null;
            this.__timesUsed = null;
            this.__savedSize__ = null;
            this.__buckets__ = null;
            this.__deconstructTime = Date.now();
            this.__destroying__ = null;
            ItemClass.super.deconstructor.call(this);
        },

        /**
         * Validates arguments before calling private __destroy__
         * @param   {Boolean} __force bypasses lock state
         * @returns {Object} this instance
         */
        destroy: function(__force) {
            if (!$assertful.optTrue(__force)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Boolean|null| only please', arguments));
                return this;
            }

            if (!this.isSafeToDestroy() && __force !== true) {
                $logger('Unable to destroy cache[' + this.__hash + '] - item is locked or has a locked link');
                return this;
            }

            this.__destroy__();
            return this;
        },

        /**
         * Emits event to be picked up by all associated pools/buckets
         * Unlocked and unlinks item item
         * @returns {Object} this instance
         * @private
         * @protected
         */
        __destroy__: function() {
            this.unlock();
            this.emit('destroyed');
            this.deconstructor();
            return this;
        },

        /**
         * Add new bucket to the list of linked buckets
         * @param {Object} __bucket Instance of Bucket
         * @returns {Object} this instance
         */
        addBucket: function(__bucket) {
            if (this.__buckets__.indexOf(__bucket) < 0) {
                this.__buckets__.push(__bucket);
                this.verbose('Linked to node: ' + __bucket.getName());
            }
            return this;
        },

        /**
         * Removed bucked instance from the list of linked buckets - if found!
         * @param   {Object} __bucket instance of bucket
         * @returns {Object} this instance
         */
        removeBucket: function(__bucket) {
            var _index = this.__buckets__.indexOf(__bucket);
            if (_index >= 0) {
                this.__buckets__.splice(_index, 1);
                this.verbose('Unlinked from node: ' + __bucket.getName());
            }
            return this;
        },

        /**
         * Removes item from buckets
         * @param   {Boolean} __force if true ignores bucket lock flag
         * @returns {Object}         this instance
         */
        removeFromBuckets: function(__force) {
            var _i, _bucket, _clonedArray;

            if (!this.isLinked()) {
                return this;
            }

            _clonedArray = this.__buckets__.slice(0);

            for (_i = 0; _i < _clonedArray.length; _i += 1) {
                _bucket = _clonedArray[_i];
                //if node determines items is used or item is manually is marked as used
                if (__force === true || !_bucket.isLocked()) {
                    //always force the lock bypass
                    _bucket.removeItem(this, true);
                }
            }
            return this;
        },

        /**
         * Ability to store, save size as at some point cache may request it.
         * @param   {Number} __size save size
         */
        saveSize: function(__size) {
            this.__savedSize__ = __size;
        },

        /**
         * Emits "sizeUpdate" and calls .saveSize() of current memory
         * @returns {Object} this instance
         */
        __onSizeChange__: function() {
            this.emit('sizeUpdate');
            //after all listeners had a chance to propagate and update their data - save new memory size
            this.saveSize(this.getMemorySize());
            return this;
        },

        /**
         * Updates last used time to Date.now()
         * and number of times used
         * @returns {Object} this instance
         */
        update: function() {
            //increment times used
            this.__timesUsed += 1;
            this.__lastUsedTime = Date.now();
            return this;
        },



        //----------------------------------------------      DATA      --------------------------------------------------------



        /**
         * A placeholder for subclass methods
         * MUST override when implementing
         */
        clearData: function() {
            this.emit('error', $error($error.TYPE.CORRUPTED_ITEM, '.clearData Method must be defined'));
        },


        /**
         * Calls __releaseData__() only if valid arguments supplied
         * @param   {Boolean} __force if true - calls .clearData() on locked items
         * @param   {Boolean} __doNotEmiitEvent if true - event "sizeUpdate" is not emitted
         * @returns {Object}  this instance
         */
        releaseData: function(__force, __doNotEmiitEvent) {
            if (!$assertful.optBoolean(__force) || !$assertful.optBoolean(__doNotEmiitEvent)) {
                return this.emit('error', $error($error.TYPE.ARGUMENTS, 'invalid arguments', arguments));
            }

            return this.__releaseData__(__force, __doNotEmiitEvent);
        },

        /**
         * Warning! No validation of arguments performed, use at your own risk!
         * Calls clearData() only for lock & force | !lock states.
         * Emits "sizeUpdate" event for pools to update their cache status
         * @param   {Boolean} __force if true - calls .clearData() on locked items
         * @param   {Boolean} __doNotEmiitEvent if true - event "sizeUpdate" is not emitted
         * @returns {Object}  this instance
         */
        __releaseData__: function(__force, __doNotEmiitEvent) {
            if (!__force && this.__locked__) {
                return this;
            }

            this.clearData();

            return this;
        },



        //----------------------------------------------      TESTERS      --------------------------------------------------------



        /**
         * Tests if any linkage exists (bucket)
         * @returns {Boolean} true if one or more link exist
         */
        isLinked: function() {
            if (this.__buckets__ <= 0) {
                return false;
            }

            return true;
        },

        /**
         * Tests if a LOCKED link exists (locked bucket)
         * @returns {Boolean} true if locked link exist
         */
        hasLockedLink: function() {
            var _i, _bucket;

            if (!this.isLinked()) {
                return false;
            }

            for (_i = 0; _i < this.__buckets__.length; _i += 1) {
                _bucket = this.__buckets__[_i];
                //if node determines items is used or item is manually is marked as used
                if (_bucket.isLocked()) {
                    return true;
                }
            }

            return false;
        },

        /**
         * Tests for locked links and a lock own flag
         * @returns {Boolean} true if no LOCKED links exist and item is NOT LOCKED
         */
        isSafeToDestroy: function() {
            return !this.hasLockedLink() && !this.isLocked();
        },

        /**
         * Tests provided hash and compares it to own key
         * @param   {Mixed}  __hash hash
         * @returns {Boolean} true if this object is associated with provided key
         */
        isOfHash: function(__hash) {
            return this.__hash === __hash;
        },

        /**
         * Assigned .__lifespan value
         * @param {Number} __timeMs duration in MS
         * @returns {Object} this instance
         */
        setLifespan: function(__timeMs) {
            if ($assertful.number(__timeMs)) {
                this.__lifespan = __timeMs;
            } else {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to set lifespan of item! Invalid lifespan argument supplied'));
            }
            return this;
        },

        /**
         * Cheks lifetime from creation against .__lifespan or provided override argument
         * @returns {Boolean} true if expired
         */
        isExpired: function() {
            if (!$assertful.number(this.__lifespan)) {
                return false;
            }
            return this.getLifeTime() > this.__lifespan;
        },

        /**
         * Checks lifetime from creation against .__lifespan or provided override argument
         * @param {Number} __lifespan Optional duration in ms
         * @returns {Boolean} true if expired
         */
        isOlderThan: function(__lifespan) {
            if (!$assertful.positive(__lifespan)) {
                // this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Unable to read argument! Invalid lifespan argument supplied'));
                return false;
            }

            return this.getLifeTime() > __lifespan;
        },


        isLocked: function() {
            return this.__locked__ === true;
        },



        //----------------------------------------------      GETTERS      --------------------------------------------------------



        /**
         * Hash getter
         * @returns {String|Number} item hash
         */
        getHash: function() {
            return this.__hash;
        },

        /**
         * Type Cache Item getter
         * @returns {Object} POOL type (see Config of pool and a subclass of item)
         */
        getType: function() {
            return this.__type__;
        },

        /**
         * returns own cargo. Calls update method to ensure last used time is updated
         */
        getData: function() {
            this.emit('error', $error($error.TYPE.CORRUPTED_ITEM, '.getData Method must be defined'));
        },

        /**
         * If not overwritten - will emit error
         * @param {Object} __type MEMORY TYPE
         */
        getMemorySize: function(__type) {
            this.emit('error', $error($error.TYPE.CORRUPTED_ITEM, 'invalid arguments', arguments));
        },

        /**
         * Return last used time (ms)
         * If .update method was never explicitly called - the value is the same as time created
         * @returns {Number} ms from epoch
         */
        getLastUsedTime: function() {
            return this.__lastUsedTime;
        },

        /**
         * Return time in ms of time this object was initially created (see constructor)
         * @returns {Number} time in ms from epoch
         */
        getTimesUsed: function() {
            return this.__timesUsed;
        },

        /**
         * Returns duration in ms from the time created till now
         * @returns {Number} duration since creation
         */
        getLifeTime: function() {
            return Date.now() - this.__timeCreated;
        },

        /**
         * Return time in ms of time this object was initially created (see constructor)
         * @returns {Number} time in ms from epoch
         */
        getTimeCreated: function() {
            return this.__timeCreated;
        },

        /**
         * Ability to store, save size as at some point cache may request it.
         * @returns {Object} this instance
         */
        getSavedSize: function() {
            return this.__savedSize__ || 0;
        },

        /**
         * Return item.__lifespan value
         * @returns {Number|Null} Number is defined, else | null
         */
        getLifespan: function() {
            return this.__lifespan;
        },



        //----------------------------------------------      LOCK      --------------------------------------------------------



        /**
         * Calls .lock|.unlock for a Boolean flag
         * @param {Boolean} __bool if true calls .lock(), false calls .unlock(), else - DOES NOTHING
         * @returns {Object} this instance
         */
        setLock: function(__bool) {
            if (!$assertful.boolean(__bool)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Only Boolean expected', arguments));
                return this;
            }
            if (__bool === true) {
                this.lock();
            } else if (__bool === false) {
                this.unlock();
            }
            return this;
        },

        /**
         * Set lock to true
         * @returns {Object} this instance
         */
        lock: function() {
            this.__locked__ = true;
            return this;
        },
        /**
         * Set lock to false
         * @returns {Object} this instance
         */
        unlock: function() {
            this.__locked__ = false;
            return this;
        }
    };

    ItemClass = $eventful.emitter.subclass('CacheItem', ItemClass);
    ItemClass.DEFAULT_CONFIG = DEFAULT_CONFIG;
    return ItemClass;
});
