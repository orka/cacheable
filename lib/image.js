/**
 * Anything cacheable is a cacheItem.
 * Use this class to extent your objects Image, Meta, Texture etc...
 */
define(['p!-cacheable/item',
    'p!-cacheable/config',
    'p!-image'
], function($CacheItem,
    $config,
    $Image) {
    'use strict';

    //subclass from item
    var CacheImage = $CacheItem.subclass('CacheImage', {
        /**
         * Class constructor
         * a a subclass of cache item class with a image API mixed into it
         * @param   {String} __url       image URL
         * @param   {Object} __options   item and image class params ex:{locked:Boolean, image:instance as a fall-back, lifespan:Number}
         * @param   {Object} __throttler network throttler instance to be used to throttle all the network requests
         */
        constructor: function(__url, __options, __throttler) {
            //evoke prototype constructor of Image
            $Image.image.prototype.constructor.call(this, __url, __options, __throttler);
            CacheImage.super.constructor.call(this, $config.TYPE.SYSTEM, this.url, __options);
        },
        deconstructor: function() {
            CacheImage.super.deconstructor.call(this);
            //evoke prototype deconstructor of Image
            $Image.image.prototype.deconstructor.call(this);
        }
    });

    //!IMPORTANT - MIXIN Image APIs!
    CacheImage.mixin($Image.image.prototype);
    //override image
    CacheImage.mixin({
        /**
         * override get data method
         * @returns {Number} a file size of image / compressed
         */
        getMemorySize: function () {
            return this.getFileSize();
        },

        getScaledSize: function () {
            return this.scaledSize;
        }
    });


    CacheImage.getDefaultUrl = $Image.image.getDefaultUrl;
    CacheImage.transformURL = $Image.image.transformURL;
    CacheImage.DEFAULT_URL = $Image.image.DEFAULT_URL;
    CacheImage.DEFAULT_IMAGE = $Image.image.DEFAULT_IMAGE;
    CacheImage.DEFAULT_CONFIG = {image: $Image.image.DEFAULT_CONFIG, cacheItem: $CacheItem.DEFAULT_CONFIG};
    return CacheImage;
});
