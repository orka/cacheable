/**
 * Configuration of all cache objects which includes:
 * - Expiry instruction
 *
 */
define(['p!-cacheable/pool',
    'p!-assertful',
    'p!-cacheable/error',
    'p!-defineclass',
    'p!-logger'
], function($Pool,
    $assertful,
    $error,
    $classify,
    $logger) {
    'use strict';
    var CacheConfig;
    var isConfigured = false;
    var masterConfigToken = Math.random() * Math.random();
    var MAINTENANCE_INTERVAL = 1000;
    var LIFESPAN = 1000 * 60;
    var CAPACITY = 1000;
    var MIN_CAPACITY = 100;
    // CONF Example {
    //     type: 'SYSTEM',
    //     | 'VIDEO' | 'HEAP'
    //     capacity: 100000(KB)
    //     fifo: true,
    //     lifespan: 10000(ms)
    //     lfu: true && !fifo
    // }

    var DEFAULT_CONFIG = {
        SYSTEM: {
            capacity: CAPACITY, // (KB)
            fifo: true, //first in first out
            lifespan: LIFESPAN, //(ms)
            lfu: false, //least frequently used
            lru: false, //least recently used
            maintenance: MAINTENANCE_INTERVAL // 1 sec
        },
        VIDEO: {
            capacity: CAPACITY, // (KB)
            fifo: true, //first in first out
            lifespan: LIFESPAN, //(ms)
            lfu: false, //least frequently used
            lru: false, //least recently used
            maintenance: MAINTENANCE_INTERVAL // 1 sec
        },
        HEAP: {
            capacity: CAPACITY, // (KB)
            fifo: true, //first in first out
            lifespan: LIFESPAN, //(ms)
            lfu: false, //least frequently used
            lru: false, //least recently used
            maintenance: MAINTENANCE_INTERVAL // 1 sec
        }
    };

    //define MasterTypes
    var TYPE = {
        SYSTEM: {
            name: 'SYSTEM',
            descr: 'System Memory type [IMAGES] used by cacheable pools and items'
        },
        VIDEO: {
            name: 'VIDEO',
            descr: 'VIDEO Memory type [GPU] used by cacheable pools and items'
        },
        HEAP: {
            name: 'HEAP',
            descr: 'HEAP Memory type [JSHEAP] used by cacheable pools and items'
        }
    };
    //report and capacity units
    var UNITS = 'KB';


    function logStats() {
        var _keys = Object.keys(TYPE);
        var _i, _pool;

        for (_i = 0; _i < _keys.length; _i += 1) {
            _pool = TYPE[_keys[_i]].pool;
            _pool.logStats();
        }
    }

    function isValidConfig(__config) {
        return $assertful.object(__config) && $assertful.positive(__config.capacity);
    }

    /**
     * Initial Master configuration of all cache types
     * @param   {Array} __masterConfig Project configured cache params - see example above
     */
    function configureMasterCache(__masterConfig) {
        var _i, _typeConf, _pool, _conf, _type, _keys, _key;

        if (!$assertful.object(__masterConfig)) {
            throw $error($error.TYPE.TYPE_ERROR, 'Master Configuration must be an Object!', __masterConfig);
        }

        if (!$assertful.object(__masterConfig.SYSTEM) && !$assertful.object(__masterConfig.VIDEO) && !$assertful.object(__masterConfig.HEAP)) {
            throw $error($error.TYPE.TYPE_ERROR, 'Specify configuration target (SYSTEM | VIDEO | HEAP)', __masterConfig);
        }

        if (isConfigured) {
            throw $error($error.TYPE.CONFIG, 'Master Pool already configured');
        }

        _keys = Object.keys(__masterConfig);

        for (_i = 0; _i < _keys.length; _i += 1) {
            _key = _keys[_i];
            _typeConf = __masterConfig[_key];

            //match the object name against type to identify the type
            if (!TYPE[_key]) {
                throw $error($error.TYPE.TYPE_ERROR, 'Config not valid');
            }

            if (!isValidConfig(_typeConf)) {
                throw $error($error.TYPE.TYPE_ERROR, 'capacity is not defiend', _typeConf);
            }

            //no lees than min capacity to avoid swarm of meaningless configurations
            if (_typeConf.capacity < MIN_CAPACITY) {
                throw $error($error.TYPE.TYPE_ERROR, 'Mix capacity allowed is "' + MIN_CAPACITY + '"', _typeConf);
            }

            _type = TYPE[_key];

            if (_type.pool) {
                throw $error($error.TYPE.CONFIG, 'Master Pool cannot be defined twice or overwritten!', _typeConf);
            }

            _conf = CacheConfig(_type, _typeConf, masterConfigToken);
            _pool = $Pool(_conf, 'Master');
            _type.pool = _pool;
            //report
            $logger.verbose('Master Cache "' + _key + '" was configured: {capacity: ' + _conf.capacity + ', fifo: ' + _conf.fifo + ', lfu: ' + _conf.lfu + ', lifespan: ' + _conf.lifespan + '-ms}', $logger.TAGS.CACHE);

            if (_typeConf === DEFAULT_CONFIG[_key]) {
                $logger.verbose('Master Cache "' + _key + '" has a default configuration! Use custom configurations', $logger.TAGS.CACHE);
            }
        }

        isConfigured = true;
    }

    /**
     * Clears all master pools
     */
    function teardownMasterCache() {
        var _i, _type, _keys, _key;
        _keys = Object.keys(TYPE);

        for (_i = 0; _i < _keys.length; _i += 1) {
            _key = _keys[_i];

            _type = TYPE[_key];

            if (_type.pool) {
                _type.pool.deconstructor();
                _type.pool = null;
                $logger.verbose('Master Cache "' + _key + '" was destroyed', $logger.TAGS.CACHE);
            }
        }

        isConfigured = false;
    }

    /**
     * Class API
     * @type {Object}
     */
    CacheConfig = {
        /**
         * Sets all parameters required by pool
         * @param {Object} __type Pool type (see TYPE)
         * @param {Object} __options configuration options
         * @param {Object} __token token identifying master pool configuration
         */
        constructor: function(__type, __options, __token) {
            if (!isConfigured && __token !== masterConfigToken) {
                throw $error($error.TYPE.TYPE_ERROR, 'Cache was not configured');
            }
            //identify type of memory
            switch (__type) {
                case TYPE.SYSTEM:
                case TYPE.VIDEO:
                case TYPE.HEAP:
                    this.__type__ = __type;
                    break;
                default:
                    throw $error($error.TYPE.TYPE_ERROR, 'Unknown type specified', __type);
            }

            if (!isValidConfig(__options)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Invalid configuration options', __options);
            }

            if (!$assertful.optBoolean(__options.fifo) || !$assertful.optBoolean(__options.lfu) || !$assertful.optBoolean(__options.lru) || !$assertful.optPint(__options.lifespan) || !$assertful.optPint(__options.maintenance)) {
                throw $error($error.TYPE.TYPE_ERROR, 'Invalid parameters of options! Use Boolean and Number', __options);
            }

            //some of the flags are mutually exclusive!
            if (__options.fifo && __options.lfu && __options.lru) {
                throw $error($error.TYPE.CONFIG, 'Mutually exclusive arguments are not allowed!');
            }

            // if (isConfigured && __options.capacity > this.__type__.pool.config.getCapacity()) {
            //     throw $error($error.TYPE.CONFIG, 'Capacity of child configurations cannot exceed that of master!', __options);
            // }

            //
            this.fifo = __options.fifo; //First In First Out
            this.lru = __options.lru; //First In First Out
            this.lfu = __options.lfu; // Least Frequently Used
            this.lifespan = __options.lifespan || null;
            this.capacity = __options.capacity; //KB
            this.maintenance = __options.maintenance || MAINTENANCE_INTERVAL;
            //normalize rules to have FIFO be set as default if nothing else was set
            if (!this.fifo && !this.lfu && !this.lru) {
                this.lru = true; //default
                $logger.verbose('"Least Reacently used" (LRU) cache mode set', $logger.TAGS.CACHE);
            }
        },
        deconstructor: function() {
            this.fifo = null;
            this.lifespan = null;
            this.lfu = null;
            this.capacity = null;
            this.__type__ = null;
        },
        isFIFO: function() {
            return this.fifo === true;
        },
        isLFU: function() {
            return this.lfu === true;
        },
        isLRU: function() {
            return this.lru === true;
        },
        getLifespan: function() {
            return this.lifespan;
        },
        getCapacity: function() {
            return this.capacity;
        },
        getMemoryType: function() {
            return this.__type__;
        },
        getUnits: function() {
            return UNITS;
        },
        getType: function() {
            return this.__type__;
        },
        getMaintInteval: function() {
            return this.maintenance;
        },
        isMasterType: function(__pool) {
            return this.__type__.pool === __pool;
        }
    };

    CacheConfig = $classify('CacheConfig', CacheConfig);
    CacheConfig.TYPE = Object.freeze(TYPE);
    CacheConfig.DEFAULT_CONFIG = Object.freeze(DEFAULT_CONFIG);
    CacheConfig.UNITS = UNITS;
    CacheConfig.init = configureMasterCache;
    CacheConfig.deinit = teardownMasterCache;
    CacheConfig.stats = logStats;
    Object.freeze(CacheConfig);
    return CacheConfig;
});
