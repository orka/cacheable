define([
    './lib/item',
    './lib/image',
    './lib/texture',
    './lib/metadata',
    './lib/pool',
    './lib/bucket',
    './lib/config',
    './lib/error'
], function(
    $item,
    $image,
    $texture,
    $metadata,
    $pool,
    $bucket,
    $config,
    $error
) {
    'use strict';
    return {
        item: $item,
        image: $image,
        texture: $texture,
        metadata: $metadata,
        pool: $pool,
        bucket: $bucket,
        config: $config,
        error: $error
    };
});
